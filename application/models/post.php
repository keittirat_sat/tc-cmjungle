<?php

class Post extends CI_Model {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
    }

    public function approve($post_id) {
        return $this->ci->db->where('guest_id', $post_id)->update('guestbook', array('guest_approve' => 1));
    }

    public function get_gustbook($status, $limit = 0, $offset = 0, $order = 'desc') {
        $this->ci->db->from('guestbook')->where('guest_approve', $status);
        if ($offset != 0 && $limit != 0) {
            $this->ci->db->limit($limit, $offset);
        } else if ($limit != 0) {
            $this->ci->db->limit($limit);
        }

        return $this->ci->db->order_by('guest_timestamp', $order)->get()->result();
    }

    public function save_guestbook($data) {
        return $this->ci->db->insert('guestbook', $data);
    }

    public function get_number_of_guestbook($status) {
        return $this->ci->db->from('guestbook')->where('guest_approve', $status)->count_all_results();
    }

    public function del_guestbook($guest_id) {
        if ($guest_id) {
            return $this->ci->db->where('guest_id', $guest_id)->delete('guestbook');
        } else {
            return false;
        }
    }

    public function get_album_info_by_id($album_id) {
        $album = $this->ci->db->from('album')->where('album_id', $album_id)->get()->result_array();

        $album = $album[0];

        $album['image'] = $this->ci->db->from('image')->where('album_id', $album_id)->get()->result();

        return $album;
    }

    public function get_img_by_type($limit) {
        return $this->ci->db->select('image.*')->from('image')->join('album', 'image.album_id = album.album_id')->limit($limit)->get()->result();
    }

    public function get_album($status) {
        $album = array();
        $all = $this->ci->db->select('album_id')->from('album')->where('album_approve', $status)->order_by('album_id', 'desc')->get();
        foreach ($all->result() as $rec) {
            $temp = $this->get_album_info_by_id($rec->album_id);
            array_push($album, $temp);
        }
        return $album;
    }

    public function get_album_info_by_status($status) {
        return $this->ci->db->from('album')->where('album_approve', $status)->get()->result();
    }

    public function get_num_album($status) {
        return $this->ci->db->select('album_id')->from('album')->where('album_approve', $status)->order_by('album_id', 'desc')->count_all_results();
    }

    public function get_publish_album() {
        return $this->get_album(PUBLISH_ALBUM);
    }

    public function get_private_album() {
        return $this->get_album(PRIVATE_ALBUM);
    }

    public function delete_img_by_id($id) {
        $img = $this->ci->db->from('image')->where('img_id', $id)->get();
        $path = './uploads';
        foreach ($img->result() as $rec) {
            $img_path = $path . "/" . $rec->album_id . "/" . $rec->img_name;
            $img_path_mini = $path . "/" . $rec->album_id . "/mini_" . $rec->img_name;
            unlink($img_path);
            if ($rec->album_id != SLIDE_IMAGE) {
                unlink($img_path_mini);
            }
        }
        return $this->ci->db->where('img_id', $rec->img_id)->delete('image');
    }

    public function get_album_by_id($album_id) {
        $img = $this->ci->db->from('image')->where('album_id', $album_id)->get();
        return $img->result_array();
    }

    public function delete_album($album_id) {
        $img = $this->get_album_by_id($album_id);
        foreach ($img as $each_img) {
            $this->delete_img_by_id($each_img['img_id']);
        }
        return $this->ci->db->where('album_id', $album_id)->delete('album');
    }

    public function get_album_last($table, $field_order) {
        $from_db = $this->ci->db->from($table)->order_by($field_order, 'desc')->limit(1)->get()->result();
        return $from_db[0];
    }

    public function auto_del_temp() {
        $u = date('U');
        $u -= 3600;
        $album = $this->get_album_info_by_status(PRIVATE_ALBUM);
        foreach ($album as $each_album) {
            if ($each_album->album_date <= $u) {
                if (!trim($each_album->album_name)) {
                    $this->delete_album($each_album->album_id);
                }
            }
        }
    }

    public function reserv_post() {
        $post = array(
            'post_title' => '',
            'post_detail' => '',
            'post_date' => date('U'),
            'post_status' => PRIVATE_POST,
            'post_thumbnail' => 0
        );

        $f = $this->ci->db->insert('post', $post);

        if ($f) {
            return $this->get_album_last('post', 'post_id');
        } else {
            return false;
        }
    }

    public function get_post_info_id($post_id) {
        $temp = $this->ci->db->from('post')->where('post_id', $post_id)->get()->result();
        return $temp[0];
    }

    public function reserve_album() {
        $album = array(
            'album_date' => date('U'),
            'album_approve' => PRIVATE_ALBUM,
            'album_name' => '',
            'album_detail' => ''
        );

        $f = $this->ci->db->insert('album', $album);

        if ($f) {
            return $this->get_album_last('album', 'album_id');
        } else {
            return false;
        }
    }

    public function update_gallery($album_obj, $album_id) {
        return $this->ci->db->where('album_id', $album_id)->update('album', $album_obj);
    }

    public function get_album_info_id($album_id) {
        $temp = $this->ci->db->from('album')->where('album_id', $album_id)->get()->result();
        return $temp[0];
    }

    public function get_image_by_id($img_id) {
        $img = $this->ci->db->from('image')->where('img_id', $img_id)->get()->result();
        if (count($img)) {
            return array(
                'mini_url' => site_url('uploads/' . $img[0]->album_id . '/mini_' . $img[0]->img_name),
                'url' => site_url('uploads/' . $img[0]->album_id . '/' . $img[0]->img_name)
            );
        } else {
            return false;
        }
    }

    public function insert_img($img) {
        return $this->ci->db->insert('image', $img);
    }

    public function get_last_img($album_id = null) {
        $this->ci->db->from('image');
        if ($album_id) {
            $this->ci->db->where('album_id', $album_id);
        }
        $img = $this->ci->db->order_by('img_id', 'desc')->limit(1)->get()->result();
        return $img[0];
    }

//    Pin Section

    public function newpin($data) {
        return $this->ci->db->insert('meta_link', $data);
    }

    public function get_all_pin($limit = null, $offset = null) {

        if ($limit != null) {
            if ($offset != null) {
                $this->ci->db->limit($limit, $offset);
            } else {
                $this->ci->db->limit($limit);
            }
        }

        return $this->ci->db->select('*')->from('meta_link')->order_by('link_id', 'desc')->get()->result();
    }

    public function get_number_of_pin() {
        return $this->ci->db->from('meta_link')->count_all_results();
    }

    public function get_pin($pin_id) {
        $data = $this->ci->db->select('*')->from('meta_link')->where('link_id', $pin_id)->order_by('link_id', 'desc')->get();
        $temp = $data->result();
        return $temp[0];
    }

    public function del_pin($pin_id) {
        return $this->ci->db->where('link_id', $pin_id)->delete('meta_link');
    }

    public function updatepin($data, $pin_id) {
        return $this->ci->db->where('link_id', $pin_id)->update('meta_link', $data);
    }

    public function get_all_slide() {
        $slide = $this->get_album_by_id(SLIDE_IMAGE);
        foreach ($slide as &$each_slide) {
            $each_slide['url'] = site_url("uploads/" . SLIDE_IMAGE . "/" . $each_slide['img_name']);
        }
        return $slide;
    }

    public function get_all_post($post_status, $order = 'desc', $limit = null) {
        if ($limit != null) {
            $this->ci->db->limit($limit);
        }

        return $this->ci->db->from('post')->join('image', 'post.post_thumbnail = image.img_id', 'left')->where('post.post_status', $post_status)->order_by('post.post_id', $order)->get()->result();
    }

    public function update_post($post_obj, $post_id) {
        return $this->ci->db->where('post_id', $post_id)->update('post', $post_obj);
    }

    public function del_post($post_id) {
        return $this->ci->db->where('post_id', $post_id)->delete('post');
    }

    public function get_all_lang() {
        return $this->ci->db->from('language')->where_not_in('lang_name', "")->get()->result();
    }

    public function reserve_language() {
        $temp = array(
            'lang_name' => '',
            'lang_info' => ''
        );

        $f = $this->ci->db->insert('language', $temp);

        if ($f) {
            return $this->get_album_last('language', 'lang_id');
        } else {
            return false;
        }
    }

    public function update_lang($lang, $lang_id) {
        return $this->ci->db->where('lang_id', $lang_id)->update('language', $lang);
    }

    public function del_lang($lang_id) {
        return $this->ci->db->where('lang_id', $lang_id)->delete('language');
    }

    public function get_lang_by_id($lang_id) {
        $temp = $this->ci->db->from('language')->where('lang_id', $lang_id)->get()->result();
        return $temp[0];
    }

}
