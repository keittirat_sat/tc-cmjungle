<?php

class Auth extends CI_Model {

    private $ci;

    public function __construct() {
        parent::__construct();
        $this->ci = get_instance();
    }

    public function login($user, $pass) {
        $exist = $this->ci->db->from('usergroup')->where('username', $user)->where('password', $pass)->get()->result();
        if (count($exist)) {
            $temp = $exist[0];
            $session['username'] = $temp->username;
            $session['level'] = $temp->level;
            $this->ci->session->set_userdata($session);
            return true;
        } else {
            return false;
        }
    }

}
