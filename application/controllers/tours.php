<?php

class Tours extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template->set('nav_url', site_url('management'));
        $this->template->set('all_lang', $this->post->get_all_lang());
        $l = $this->session->userdata('lang_id');
        $l = is_numeric($l) ? $l : 1;
        $lang = "lang_" . $l;
        $this->template->set('current_lang', $lang);
        $this->template->set('lang_info', $this->post->get_lang_by_id($l));
        $this->template->set('gallery_menu', false);
        $this->template->set('trip_menu', false);
        $this->template->set('header', $this->template->element('tours/header.php'));
        $this->template->set('footer', $this->template->element('tours/footer.php'));
        //        $this->output->cache(60);
    }

    public function index()
    {
        $css = css_asset('nivo-slider.css') . css_asset('themes/default/default.css') . css_asset('tours.css');
        $js = js_asset('jquery.nivo.slider.pack.js');
        $this->template->set('title', 'Welcome to Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->set('slide', $this->post->get_all_slide());
        $this->template->set('post', $this->post->get_all_post(PUBLISH_POST, 'desc', 10));
        $this->template->set('guestbook', $this->post->get_gustbook(GUESTBOOK_APPROVED, 3));
        $this->template->set('pin', $this->post->get_all_pin(3));
        $this->template->set('sidebar', $this->template->element('tours/sidebar.php'));
        $this->template->load();
    }

    public function contactus()
    {
        $css = css_asset('tours.css');
        $js = js_asset('jquery-ui-1.8.18.min.js') . js_asset('jquery.form.js') . js_asset('jquery.ui.map.js') . '<script src="http://maps.google.com/maps/api/js?key=AIzaSyA4UzDRbjkkxOl9Hzgden0RCDtUJCwLOVE&sensor=true" type="text/javascript"></script>';
        $this->template->set('title', 'Contact Us | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->load();
    }

    public function aboutus()
    {
        $css = css_asset('tours.css');
        $this->template->set('title', 'About Us | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->load();
    }

    public function trip($post_id = null, $post_title = null)
    {
        $post = $this->post->get_all_post(PUBLISH_POST);
        $css = css_asset('tours.css');
        $this->template->set('title', 'Program tours | All Trip | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('post', $post);
        $this->template->set('post_id', $post_id);
        $this->template->set('trip_menu', true);
        $this->template->load();
    }

    public function gallery()
    {
        $css = css_asset('tours.css');
        $this->template->set('title', 'Gallery | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('album', $this->post->get_album(PUBLISH_ALBUM));
        $this->template->load();
    }

    public function album($album_id, $album_name)
    {
        $album = $this->post->get_album_info_by_id($album_id);
        $css = css_asset('tours.css') . css_asset('ekko-lightbox.min.css');
        $js = js_asset('ekko-lightbox.min.js');
        $this->template->set('title', $album['album_name'] . ' | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->set('album', $album);
        $this->template->set('gallery_menu', true);
        $this->template->load();
    }

    public function guestbook()
    {
        $css = css_asset('tours.css');
        $js = js_asset('jquery.form.js');
        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 0;
        $total_post = $this->post->get_number_of_guestbook(GUESTBOOK_APPROVED);
        $total = 10;
        $this->template->set('title', 'Guesbook | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->set('page', $page);
        $this->template->set('total_page', ceil($total_post / $total));
        $this->template->set('guestbook', $this->post->get_gustbook(GUESTBOOK_APPROVED, $total, $page * $total));
        $this->template->load();
    }

    public function review()
    {
        $css = css_asset('nivo-slider.css') . css_asset('themes/default/default.css') . css_asset('tours.css');
        $js = js_asset('jquery.nivo.slider.pack.js');
        $page = $this->input->get('page');
        $page = is_numeric($page) ? $page : 0;
        $total = 10;
        $offset = $page * $total;

        $this->template->set('title', 'Referer review | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->set('guestbook', $this->post->get_gustbook(GUESTBOOK_APPROVED, 3));
        $this->template->set('pin', $this->post->get_all_pin(3));
        $this->template->set('review', $this->post->get_all_pin($total, $offset));
        $this->template->set('total_page', ceil($this->post->get_number_of_pin() / $total));
        $this->template->set('page', $page);
        $this->template->set('sidebar', $this->template->element('tours/sidebar.php'));
        $this->template->load();
    }

    public function pin_review($pin_id, $pin_title = null)
    {
        $css = css_asset('nivo-slider.css') . css_asset('themes/default/default.css') . css_asset('tours.css');
        $js = js_asset('jquery.nivo.slider.pack.js');
        $post = $this->post->get_pin($pin_id);
        $this->template->set('title', $post->link_title . ' | Referer review | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->set('guestbook', $this->post->get_gustbook(GUESTBOOK_APPROVED, 3));
        $this->template->set('pin', $this->post->get_all_pin(3));
        $this->template->set('post', $post);
        $this->template->set('sidebar', $this->template->element('tours/sidebar.php'));
        $this->template->load();
    }

    public function guestalbum()
    {
        $css = css_asset('tours.css');
        $js = js_asset('jquery.form.js');
        $this->template->set('title', 'Elephant Training Trip | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('js', $js);
        $this->template->set('gallery_menu', true);
        // if (loged(1)) {
        $url = gallery_api . "all-gallery?all=0&site_id=" . site_id;
        $resp_json = $this->tc_http->get($url);
        $all_gallery = json_decode($resp_json);
        $this->template->set('all_album', $all_gallery->result);
        $this->template->load("tours/guestalbum_loged.php");
        // } else {
        //     $this->template->load();
        // }
    }

    public function fullsize()
    {
        $css = css_asset('tours.css');
        $this->template->set('css', $css);
        $this->template->set('gallery_menu', true);

        // if (loged(1)) {
        $album_id = $this->input->get('album_id');

        $url = gallery_api . "picture-gallery?gallery_id={$album_id}&site_id=" . site_id;
        $resp_json = $this->tc_http->get($url);
        $album_json = json_decode($resp_json);

        $album = $album_json->result;
        $this->template->set('album', $album);
        $this->template->set('album_id', $album_id);
        $this->template->set('image', $album_json->result->gallery_picture);
        $this->template->set('title', $album->name . ' | Elephant Training Trip | Chiangmai Jungle Trekking by Toto');
        $this->template->load();
        // } else {
        //     redirect(site_url('tours/guestalbum'));
        // }
    }

    public function elephanttraining()
    {
        $css = css_asset('tours.css');
        $this->template->set('title', 'Elephant Training Trip | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('trip_menu', true);
        $this->template->load();
    }

    public function hostel()
    {
        $css = css_asset('tours.css');
        $this->template->set('title', 'Elephant Training Trip | Chiangmai Jungle Trekking by Toto');
        $this->template->set('css', $css);
        $this->template->set('trip_menu', true);
        $this->template->load();
    }
}
