<?php

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-Type: text/plain");
        header('Access-Control-Allow-Origin: *');
        $this->output->clear_all_cache();
    }

    public function login() {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');

        if ($this->auth->login($user, $pass)) {
            $data['status'] = "success";
            $data['url'] = site_url('management/home');
        } else {
            $data['status'] = "fail";
        }

        $data['username'] = $user;
        echo json_encode($data);
    }

    public function logout() {
        $this->session->unset_userdata('username');
        redirect(site_url("management"));
    }

    public function set_lang() {
        $lang_id = $this->input->get_post('lang_id');
        $this->session->set_userdata('lang_id', $lang_id);
        echo json_encode(array('status' => 'success'));
    }

    public function approve_post() {
        $post_id = $this->input->get_post('post_id');
        if ($this->post->approve($post_id)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        $data['post_id'] = $post_id;
        echo json_encode($data);
    }

    public function quick_approve() {
        $this->approve_post();
        redirect(site_url('tours/guestbook'));
    }

    public function del_guestbook() {
        $guest_id = $this->input->post('post_id');
        if ($this->post->del_guestbook($guest_id)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function quick_del() {
        $this->del_guestbook();
        redirect(site_url('tours/guestbook'));
    }

    public function del_album() {
        $album_id = $this->input->get_post('album_id');
        if ($this->post->delete_album($album_id)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function upload_img() {
        $c['file'] = $_FILES;
        $c['album_id'] = $this->input->get('album_id');
        $c['date'] = date('U');
        echo json_encode($c);
    }

    public function resize_image($image_path, $new_image, $oversize = false) {
        //Setup Image manipulation procedure
        $main_size = array(
            'source_image' => $image_path,
            'maintain_ratio' => true,
            'width' => img_l_width,
            'height' => img_l_height,
            'quality' => 100
        );

        if ($oversize) {
            $main_size['width'] = img_g_width;
            $main_size['height'] = img_g_height;
        } else {
            $main_size['wm_type'] = 'overlay';
            $main_size['wm_overlay_path'] = './assets/image/footer_logo.png';
            $main_size['wm_vrt_alignment'] = 'bottom';
            $main_size['wm_hor_alignment'] = 'right';
            $main_size['wm_opacity'] = 30;
            $main_size['wm_hor_offset'] = 10;
            $main_size['wm_vrt_offset'] = 10;
        }

        //Set profile
        $this->image_lib->initialize($main_size);

        //Resize
        $this->image_lib->resize();

        // if (!$oversize) {
        //     $this->image_lib->watermark();
        // }

        //clear old config
        $this->image_lib->clear();

//        Create thumbail
        $thumb = array(
            'source_image' => $image_path,
            'new_image' => $new_image,
            'maintain_ratio' => true,
            'width' => img_s_width,
            'height' => img_s_height
        );

        //Set profile for thumbnail
        $this->image_lib->initialize($thumb);

        //Resize
        $this->image_lib->resize();
    }

    public function relay_gallery() {
        set_time_limit(0);
        $gallery_id = $this->input->post('gallery_id');
        $path = './uploads/relay/';
        
        if (!is_dir($path)) {
            mkdir($path);
        }            
        
        @chmod($path, 0777);
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|JPG|JPEG|jpeg';
        
        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('media')) {
            /**
             * Upload Fail : upload abort
             */
            $resp['code'] = '403';
            $flag = $this->upload->display_errors();
        } else {
            /**
             * Upload Success : Prepare upload file
             */
            $flag = $this->upload->data();
            $file_name = $flag['full_path'];
            $target_url = gallery_api."upload-image";
            $resp = $this->tc_http->upload_file($target_url, $gallery_id, $file_name, 0);
            @unlink($file_name);
        }
        
        $temp = json_encode($resp);
        if($temp){
            echo $temp;
        }else{
            print_r($resp);
        }
    }

    public function upload_gallery() {
        $album_id = $this->input->get('album_id');
        $oversize = $this->input->get('oversize');
        $oversize = is_numeric($oversize) ? true : false;

        $path = './uploads/' . $album_id . '/';
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|JPG|JPEG|jpeg';

        if (SLIDE_IMAGE == $album_id) {
            $config['max_width'] = slide_width;
            $config['max_height'] = slide_height;
        }

        if (!is_dir($path)) {
            mkdir($path);
        }
        @chmod($path, 0777);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('img_upload')) {
            //Fail to upload.
            $data['status'] = 'fail';
            $flag = $this->upload->display_errors();
        } else {
            //Upload success
            $flag = $this->upload->data();

            if (SLIDE_IMAGE != $album_id) {
                //create thumb
                $image_desc_thumb = $path . "/mini_" . $flag['file_name'];
                $this->resize_image($flag['full_path'], $image_desc_thumb, $oversize);
                $data['mini_url'] = site_url("uploads/{$album_id}/mini_" . $flag['file_name']);
            }

            //Store in DB
            $this->post->insert_img(array('img_name' => $flag['file_name'], 'album_id' => $album_id));

            $img_info = $this->post->get_last_img($album_id);
            $data['img_id'] = $img_info->img_id;
            $data['status'] = 'success';
            $data['url'] = site_url("uploads/{$album_id}/" . $flag['file_name']);
        }
        $data['flag'] = $flag;
        echo json_encode($data);
    }

    public function update_gallery() {
        $post = $this->input->post();
        $album_id = $post['album_id'];
        unset($post['album_id']);

        delete_files("./uploads/{$album_id}/{$album_id}.zip");

        if ($this->post->update_gallery($post, $album_id)) {
            $data['status'] = "success";
            $data['url'] = $post['album_approve'] == PUBLISH_GUEST ? site_url("management/new_guest_album?album_id={$album_id}") : site_url("management/album_operation?album_id={$album_id}");
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function del_img() {
        $img_id = $this->input->get_post('img_id');
        if ($this->post->delete_img_by_id($img_id)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }
        echo json_encode($data);
    }

    public function uploadhandle() {
        $folder = $this->input->get('folder');
        $path = './uploads/' . $folder;
        if (!is_dir($path)) {
            mkdir($path);
            chmod($path, 0777);
        }
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        $this->upload->do_upload('upload_btn');
        $d = $this->upload->data();
        $data['response'] = $d;
        $data['folder'] = $folder;
        $data['url'] = site_url("uploads/{$folder}/" . $d['file_name']);
        echo json_encode($data);
    }

    public function uploadpin() {
        $data = $this->input->post();
        if ($this->post->newpin($data)) {
            $d['status'] = 'success';
        } else {
            $d['status'] = 'fail';
        }
        $d['date'] = time();
        echo json_encode($d);
    }

    public function updatepin() {
        $post = $this->input->post();
        $link_id = $post['link_id'];
        unset($post['link_id']);
        if ($this->post->updatepin($post, $link_id)) {
            $d['status'] = 'success';
        } else {
            $d['status'] = 'fail';
        }
        $d['date'] = time();
        echo json_encode($d);
    }

    public function del_pin() {
        $pin_id = $this->input->get_post('pin_id');
        if ($this->post->del_pin($pin_id)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "reject";
        }
        echo json_encode($data);
    }

    public function updatepost() {
        $post = $this->input->post();
        $post_id = $post['post_id'];
        unset($post['post_id']);
        if ($this->post->update_post($post, $post_id)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function del_post() {
        $post_id = $this->input->get_post('post_id');
        if ($this->post->del_post($post_id)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function update_lang() {
        $post = $this->input->post();
        $post_id = $post['lang_id'];
        unset($post['lang_id']);
        if ($this->post->update_lang($post, $post_id)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function del_lang() {
        $lang_id = $this->input->post('lang_id');
        if ($this->post->del_lang($lang_id)) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        echo json_encode($data);
    }

    public function testmail() {
        /* Retrieve Value from POST */
        $pkt['recv_email'] = 'keittirat@gmail.com';
        $pkt['recv_name'] = 'ChiangMai Jungle Trekking';
        $pkt['from_name'] = 'TryCatch';
        $pkt['from_email'] = 'frankent@gmail.com';
        $pkt['subject'] = 'ทดสอบระบบ';
        $pkt['ch'] = "test-chiangmaijungletrekking.com";

        /* Process txt */
        $detail = "<p>ทดอบระบบ</p>";
        $pkt['html'] = $detail;

        $url = "http://api.cmrabbit.com/email/send";
        $recv = $this->tc_http->post($url, $pkt);
        print_r($recv);
    }

    public function sendmail() {

        /* Retrieve Value from POST */
        $from_name = $this->input->post('contact-name');
        $from_email = $this->input->post('contact-email');
        $from_whatsapp = $this->input->post('contact-whatsapp');
        $subject = $this->input->post('contact-subject');

        /* Process txt */
        $detail = $this->input->post('contact-detail');
        $detail = $this->typography->auto_typography($detail, TRUE);

        if (!empty($from_whatsapp)) {
            $detail = "Whatsapp: {$from_whatsapp}<br /><br />" . $detail;
        }

        $detail .= "<p><i>Contact form system: " . site_url() . "</i></p>";
        $detail .= "<p><i>Powered by <a href='http://www.trycatch.in.th'>TryCatch</a></i></p>";

        $recv = $this->tc_http->mail($from_email, $from_name, $subject, $detail);
        $resp = json_decode($recv);

        if ($resp->code == 200) {
            $data['status'] = "success";
        } else {
            $data['status'] = "fail";
        }
        $data['response'] = $resp;
        echo json_encode($data);
    }

    public function comment() {
        $post = $this->input->post();
        $data = array(
            'guest_name' => $post['guest-name'],
            'guest_detail' => $post['guest-comment'],
            'guest_email' => $post['guest-email'],
            'guest_approve' => GUESTBOOK_NOT_APPROVE,
            'guest_timestamp' => date('U')
        );

        if ($this->post->save_guestbook($data)) {
            $data['status'] = 'success';
        } else {
            $data['status'] = 'fail';
        }

        $last_comment = $this->post->get_gustbook(GUESTBOOK_NOT_APPROVE, 1);
        $last_comment = $last_comment[0];
        $waiting_list = $this->post->get_number_of_guestbook(GUESTBOOK_NOT_APPROVE);

        $subject = "[Chiangmai Jungle Treking] Please moderate: Guestbook: " . $post['guest-name'];
        $email_msg = "Author: " . $post['guest-name'] . "<br/>";
        $email_msg .= "Email: " . $post['guest-email'] . "<br/><br/>";
        $email_msg .= $this->typography->auto_typography($post['guest-comment'], true);
        $email_msg .= "<br/><br/>";
        $email_msg .= "Approve it: " . site_url('api/quick_approve?post_id=' . $last_comment->guest_id) . "<br/>";
        $email_msg .= "Delete it: " . site_url('api/quick_del?post_id=' . $last_comment->guest_id) . "<br/>";
        $email_msg .= "Currently {$waiting_list} comments are waiting for approval. Please visit the moderation panel:<br/>" . site_url('management/guestbook');

        $from_email = 'service@trycatch.in.th';
        $from_name = 'Trycatch Service';

        $this->tc_http->mail($from_email, $from_name, $subject, $email_msg);

        echo json_encode($data);
    }

//  public function zip_watchdog() {
//    $this->load->helper('file');
//    $all_guest = $this->post->get_album(PUBLISH_GUEST);
//    $current = date('U');
//    $data = array();
//    foreach ($all_guest as $each_album) {
//      $dir = "./uploads/{$each_album['album_id']}/{$each_album['album_id']}.zip";
//      //check current album has expired ?
//      if (($current - $each_album['album_date']) >= 3888000) {
//        $this->db->where('album_id', $each_album['album_id'])->delete('album');
//        delete_files("./uploads/{$each_album['album_id']}", true);
//      } elseif (!is_file($dir)) {
//        //if zip not exist -- add to zip list
//        $data[$each_album['album_id']]['album_id'] = $each_album['album_id'];
//      }
//    }
//
//    echo json_encode($data);
//  }
}
