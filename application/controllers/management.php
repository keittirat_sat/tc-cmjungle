<?php

class Management extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //Auto del temp
        $this->post->auto_del_temp();
        $this->template->set('nav_url', site_url());
    }

    public function index() {
        if (loged()) {
            redirect(site_url('management/home'));
        } else {
            //Login
            $js = js_asset("jquery.form.js");
            $css = css_asset("general_operation.css");

            $this->template->set('title', 'Welcome to TourInfo CMS');
            $this->template->set('js', $js);
            $this->template->set('css', $css);
            $this->template->load(null, 'plain_template');
        }
    }

    public function home() {
        if (loged()) {
            redirect(site_url('management/guestbook'));
            /*

              $this->load->library('site_analytic');
              $this->template->set('title', 'Home | Welcome to TourInfo CMS');
              $this->template->set('num_approve_guestbook', $this->post->get_number_of_guestbook(GUESTBOOK_APPROVED));
              $this->template->set('num_non_approve_guestbook', $this->post->get_number_of_guestbook(GUESTBOOK_NOT_APPROVE));
              $this->template->set('num_publish_album', $this->post->get_num_album(PUBLISH_ALBUM));
              $this->template->set('num_private_album', $this->post->get_num_album(PRIVATE_ALBUM));
              $this->template->set('visitor_pageview', $this->site_analytic->get_visitor_pageview());
              $this->template->set('keyword', $this->site_analytic->get_keyword());
              $this->template->set('site_referrer', $this->site_analytic->get_user_referrer());
              $this->template->load();
             * */
        } else {
            redirect(site_url('management'));
        }
    }

    public function guestbook() {
        if (loged()) {
            $page = $this->input->get('page');
            $mode = $this->input->get('mode');
            $mode = (is_numeric($mode)) ? $mode : GUESTBOOK_APPROVED;
            $limit = 20;
            $page = ($page >= 0) ? $page : 0;
            $offset = $limit * $page;
            $css = css_asset("general_operation.css");
            $num_approve = $this->post->get_number_of_guestbook(GUESTBOOK_APPROVED);
            $num_non_approve = $this->post->get_number_of_guestbook(GUESTBOOK_NOT_APPROVE);
            $all_post = ($mode == GUESTBOOK_APPROVED) ? $num_approve : $num_non_approve;

            $this->template->set('css', $css);
            $this->template->set('mode', $mode);
            $this->template->set('page', $page);
            $this->template->set('all_page', ceil($all_post / $limit));
            $this->template->set('app_guestbook', $this->post->get_gustbook($mode, $limit, $offset));
            $this->template->set('num_approve', $num_approve);
            $this->template->set('num_non_approve', $num_non_approve);
            $this->template->set('title', 'Guestbook Management');
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function gallery() {
        if (loged()) {
            $mode = $this->input->get('mode');
            $mode = (is_numeric($mode)) ? $mode : PUBLISH_ALBUM;
            $css = css_asset("general_operation.css");
            $this->template->set('title', 'Gallery Management');
            $this->template->set('mode', $mode);
            $this->template->set('all_album', $this->post->get_album($mode));
            $this->template->set('num_album', $this->post->get_num_album(PUBLISH_ALBUM));
            $this->template->set('num_private_album', $this->post->get_num_album(PRIVATE_ALBUM));
            $this->template->set('css', $css);
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function guestalbum() {
       if (loged()) {
           $css = css_asset("general_operation.css");
           $this->template->set('title', 'Guest Album');
           $this->template->set('css', $css);

           /**
            * Sync Gallery
            */
           $url = gallery_api . "all-gallery?all=1&site_id=".site_id;
           $resp_json = $this->tc_http->get($url);
           $all_gallery = json_decode($resp_json);

           $this->template->set('all_album', $all_gallery->result);
           $this->template->load();
       } else {
           redirect(site_url('management'));
       }
    }

    public function new_guest_album() {
        if (loged()) {
            $album_id = $this->input->get('album_id');

            if (!is_numeric($album_id)) {
                //Reserve album
                $url = gallery_api . "create-album?site_id=".site_id;
                $resp = $this->tc_http->get($url);
            } else {
                //Retrieve album
                $url = gallery_api . "picture-gallery?gallery_id={$album_id}&site_id=".site_id;
                $resp = $this->tc_http->get($url);
            }

            $album_obj = json_decode($resp);
            if ($album_obj->code == 200) {
                $album = $album_obj->result;
            } else {
                $album = array();
            }

            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js");

            $this->template->set('title', empty($album->name) ? "New Guest Album" : "Edit: {$album->name}");
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('album', $album);
            $this->template->set('image', is_numeric($album_id) ? $album->gallery_picture : array());
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function album_operation() {
        if (loged()) {
            $album_id = $this->input->get('album_id');

            if (!is_numeric($album_id)) {
                //Reserve album
                $album = $this->post->reserve_album();
            } else {
                //Retrieve album
                $album = $this->post->get_album_info_id($album_id);
            }

            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js");

            $this->template->set('title', empty($album->album_name) ? "New Guest Album" : "Edit: {$album->album_name}");
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('album', $album);
            $this->template->set('image', $this->post->get_album_by_id($album->album_id));
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function review() {
        if (loged()) {
            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js");

            $this->template->set('title', 'Client Review');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('all_pin', $this->post->get_all_pin());
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function slide() {
        if (loged()) {
            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js");

            $this->template->set('title', 'Slideshow image');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('slide', $this->post->get_all_slide());
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function post() {
        if (loged()) {
            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js");
            $post_status = $this->input->get('post_status');
            if (!is_numeric($post_status)) {
                $post_status = PUBLISH_POST;
            }
            $this->template->set('title', 'Trip detail');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('post', $this->post->get_all_post($post_status));
            $this->template->set('post_status', $post_status);
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function post_operation() {
        if (loged()) {
            $post_id = $this->input->get('post_id');
            if (!$post_id) {
                $post = $this->post->reserv_post();
            } else {
                $post = $this->post->get_post_info_id($post_id);
            }

            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js") . js_asset("tinymce/tinymce.min.js");

            $this->template->set('title', 'Trip detail');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('post_id', $post->post_id);
            $this->template->set('lang', $this->post->get_all_lang());
            $this->template->set('post', $post);
            $this->template->set('post_thumbnail', $this->post->get_image_by_id($post->post_thumbnail));
            $this->template->load('management/post');
        } else {
            redirect(site_url('management'));
        }
    }

    public function language() {
        if (loged()) {
            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js") . js_asset("tinymce/tinymce.min.js");

            $this->template->set('title', 'Languages management');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('lang', $this->post->get_all_lang());
            $this->template->load();
        } else {
            redirect(site_url('management'));
        }
    }

    public function language_operation() {
        if (loged()) {
            $css = css_asset("general_operation.css");
            $js = js_asset("jquery.form.js") . js_asset("tinymce/tinymce.min.js");

            $lang_id = $this->input->get('lang_id');
            if (is_numeric($lang_id)) {
                $lang = $this->post->get_lang_by_id($lang_id);
            } else {
                $lang = $this->post->reserve_language();
            }

            $this->template->set('title', 'Languages management');
            $this->template->set('css', $css);
            $this->template->set('js', $js);
            $this->template->set('lang_id', $lang->lang_id);
            $this->template->set('lang', $lang);
            $this->template->load('management/language');
        } else {
            redirect(site_url('management'));
        }
    }

}
