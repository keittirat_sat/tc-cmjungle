<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tc_http {

    private $ci;

    public function __construct() {
        $this->ci = get_instance();
    }

    public function post($url, $fields_string, $header = array()) {
        //open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        //execute post
        $result = curl_exec($ch); //close connection
        curl_close($ch);
        return $result;
    }

    public function get($url, $header = array()) {
        //open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if ($header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        //execute post
        $result = curl_exec($ch); //close connection
        curl_close($ch);
        return $result;
    }

    /**
     * 
     * @param email $from_email
     * @param string $from_name
     * @param string $subject
     * @param string $detail
     * @param email $recv_email optional
     * @param string $recv_name optional
     * @return raws_data
     */
    public function mail($from_email = null, $from_name = null, $subject = null, $detail = null, $recv_email = null, $recv_name = null) {
        $url = "http://api.cmrabbit.com/email/send";
        $detail = $this->ci->typography->auto_typography($detail, TRUE);
        $detail .= "<p><i>Contact form system : " . site_url() . "</i></p>";
        $detail .= "<p><i>Powered by <a href='http://www.trycatch.in.th'>TryCatch</a></i></p>";
        $data = array(
            'from_email' => $from_email,
            'from_name' => $from_name,
            'recv_email' => is_null($recv_email) ? 'chiangmaijungletrekking@gmail.com' : $recv_email,
            'recv_name' => is_null($recv_name) ? 'ChiangMai Jungle Trekking' : $recv_name,
            'subject' => $subject,
            'html' => $detail,
            'ch' => "chiangmaijungletrekking.com"
        );
        return $this->post($url, $data);
    }

    /**
     * 
     * @param type $target_url
     * @param type $gallery_id
     * @param type $file_name
     * @param type $try
     * @return type
     */
    public function upload_file($target_url, $gallery_id, $file_name, $try = 1) {
        $cFile = curl_file_create($file_name);
        $post = array(
            'media' => $cFile
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $target_url . "?gallery_id={$gallery_id}&site_id=".site_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $resp = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode == 200) {
            $resp_json = json_decode($resp, true);
        } else {
            if ($try < 15) {
                $try++;
                $this->upload_file($target_url, $gallery_id, $file_name, $try);
            }else{
                $resp_json = array('code' => 500);
            }
        }
        
        return $resp_json;
    }

}
