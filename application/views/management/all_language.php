<?php foreach ($lang as $each_lang): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title"><i class="glyphicon glyphicon-globe"></i> <?php echo $each_lang->lang_name; ?></span>
                </div>
                <div class="panel-body">
                    <label>Sample text unavailable:</label>
                    <?php echo $each_lang->lang_info; ?>
                </div>
                <div class="panel-footer txt_right">
                    <a href="<?php echo site_url("management/language_operation?lang_id={$each_lang->lang_id}"); ?>" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>