<div class='row'>
    <div action='<?php echo site_url('api/updatepost'); ?>' class='form-horizontal' method="post" id='post_update'>
        <div class='form-group'>
            <label class='col-sm-2 control-label'>Title</label>
            <div class='col-sm-10'>
                <input type='text' class='form-control' name='post_title' required="required" value='<?php echo $post->post_title; ?>'>
            </div>
        </div>
        <div class='form-group'>
            <label class='col-sm-2 control-label'>Status</label>
            <div class='col-sm-4'>
                <select class='form-control' name='post_status'>
                    <option value='<?php echo PUBLISH_POST; ?>' <?php echo (PUBLISH_POST == $post->post_status) ? "selected" : ""; ?>>Publish</option>
                    <option value='<?php echo PRIVATE_POST; ?>' <?php echo (PRIVATE_POST == $post->post_status) ? "selected" : ""; ?>>Private</option>
                </select>
            </div>
        </div>
        <form id="please_reset">
            <div class='form-group'>
                <label class='col-sm-2 control-label'>Upload</label>
                <div class='col-sm-10'>
                    <button class='btn btn-primary btn_upload' data-loading-text='Uploading...'>
                        <i class='glyphicon glyphicon-upload'></i> Insert image into post
                        <input type='file' id='new_slide'>
                    </button>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-2 control-label'>Cover image</label>
                <div class='col-sm-10'>
                    <div class="upload_cover_prt" <?php echo $post->post_thumbnail != 0 ? "style='background-image: url({$post_thumbnail['mini_url']})'" : ""; ?>>
                        <span id="cover_txt_holder"><i class="glyphicon glyphicon-upload"></i> Click to upload</span>
                        <input type='file' id='new_cover'>
                    </div>
                </div>
            </div>
        </form>
        <div class='form-group'>
            <div class='col-sm-10 col-sm-offset-2'>
                <ul class="nav nav-tabs">
                    <?php foreach ($lang as $index => $each_lang): ?>
                        <li class="<?php echo $index == 0 ? 'active' : ''; ?> post_tab" data-target="<?php echo "tiny_{$each_lang->lang_id}"; ?>"><a href="#tab_<?php echo $each_lang->lang_id ?>" data-toggle="tab"><?php echo $each_lang->lang_name ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <div class="tab-content">
                    <?php $content = (array) json_decode($post->post_detail); ?>
                    <?php foreach ($lang as $index => $each_lang): ?>
                        <div class="tab-pane <?php echo $index == 0 ? "active" : "" ?>" id="tab_<?php echo $each_lang->lang_id ?>">
                            <textarea class='tinymce_editor <?php echo "tiny_{$each_lang->lang_id}"; ?>' id="<?php echo "tiny_{$each_lang->lang_id}"; ?>" data-lang-id="<?php echo $each_lang->lang_id; ?>" name='post_detail_<?php echo $each_lang->lang_id; ?>'  style="width: 100%; resize: vertical;" rows="14">
                                <?php echo @$content["lang_" . $each_lang->lang_id]; ?>                            
                            </textarea>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class='form-group'>
            <div class='col-sm-10 col-sm-offset-2'>
                <button class='btn btn-primary' type='button' id='publish_btn' data-loading-text='Updating...'><i class='glyphicon glyphicon-check'></i> Update</button>
                <button class='btn btn-danger' type='button' data-loading-text='Deleting...'><i class='glyphicon glyphicon-trash'></i> Delete</button>
            </div>
        </div>
        <input type='submit' id='submit_form_hidden' style='display: none;'>
        <input type='hidden' value='<?php echo $post->post_id; ?>' name='post_id'>
        <input type='hidden' value='<?php echo $post->post_thumbnail; ?>' name='post_thumbnail'>
    </div>
</div>

<script type='text/javascript'>
    var img_list = [];
    tinymce.init({
        selector: "<?php
                    foreach ($lang as $index => $each_lang): echo $index == 0 ? "textarea.tiny_{$each_lang->lang_id}" : ",textarea.tiny_{$each_lang->lang_id}";
                    endforeach;
                    ?>",
        theme: "modern",
        height: "300",
        relative_urls: false,
        remove_script_host: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | media emoticons link",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        image_list: img_list
    });

    function add_html(html, target) {
        tinyMCE.get(target).execCommand('mceInsertContent', false, html);
    }

    function get_content_from_editor(target) {
        return tinyMCE.get(target).getContent();
    }

    $(function() {
        $('#new_slide').change(function() {
            console.log("Click");
            var file = this.files[0];
            var slideForm = new FormData;
            slideForm.append('img_upload', file);
            $('.btn_upload').button('loading');
            var fup = $(this).val();
            if (fup) {
                $.ajax({
                    url: '<?php echo site_url("api/upload_gallery?album_id=P" . $post->post_id); ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $('.btn_upload').text('Uploading... ' + percent + '%');
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(e) {
                        var json = $.parseJSON(e);
                        if (json.status === "success") {
                            var element = "<p>";
                            element += "<img src='" + json.url + "' class='img-responsive'>";
                            element += "</p>";
                            var target = $('.post_tab.active').attr('data-target');
                            console.log(element, target);
                            add_html(element, target);
                            $('#publish_btn').trigger('click');
                        } else {
                            console.log(e);
                            $('.btn_upload').button('reset');
                            alert('Upload fail');
                        }
                        $('.btn_upload').button('reset');
                        $('#please_reset').trigger('reset');
                    },
                    data: slideForm,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });

        $('#new_cover').change(function() {
            var file = this.files[0];
            var slideForm = new FormData;
            slideForm.append('img_upload', file);
            $('.btn_upload').button('loading');
            $.ajax({
                url: '<?php echo site_url("api/upload_gallery?album_id=P" . $post->post_id); ?>',
                type: 'POST',
                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) { // Check if upload property exists
                        xhr.upload.addEventListener('progress', function(e) {
                            var percent = parseInt(e.loaded / e.total * 100);
                            $('#cover_txt_holder').text('Uploading... ' + percent + '%');
                        }, false);
                    }
                    return xhr;
                },
                success: function(e) {
                    var json = $.parseJSON(e);
                    if (json.status === "success") {
                        $('.upload_cover_prt').css({'background-image': 'url("' + json.mini_url + '")'});
                        $('[name=post_thumbnail]').val(json.img_id);
                        $('#cover_txt_holder').html('<i class="glyphicon glyphicon-upload"></i> Click to upload');
                    } else {
                        console.log(e);
                        $('#new_slide').button('reset');
                        alert('Upload fail');
                    }
                    $('.btn_upload').button('reset');
                    $('#please_reset').trigger('reset');
                },
                data: slideForm,
                cache: false,
                contentType: false,
                processData: false
            });
        });

        $('#publish_btn').click(function() {
            var content = {};
            $('#publish_btn').button('loading');
            $('.tinymce_editor').each(function(idx, ele) {
                var lang_id = $(ele).attr('data-lang-id');
                var target_class = "tiny_" + lang_id;
                content["lang_" + lang_id] = get_content_from_editor(target_class);
            });

            var strjson = JSON.stringify(content);
            var form = new FormData();
            form.append('post_detail', strjson);
            form.append('post_status', $('[name=post_status]').val());
            form.append('post_title', $('[name=post_title]').val());
            form.append('post_id', $('[name=post_id]').val());
            form.append('post_thumbnail', $('[name=post_thumbnail]').val());

            $.ajax({
                url: '<?php echo site_url('api/updatepost'); ?>',
                type: 'POST',
                success: function(e) {
                    var json = $.parseJSON(e);
                    if (json.status === "success") {
                        location.href = location.origin + location.pathname +"?post_id="+$('[name=post_id]').val();
                    } else {
                        $('#publish_btn').button('reset');
                        alert('internal service error');
                    }
                },
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });
        });

//        $('#post_update').ajaxForm({
//            beforeSend: function() {
//                $('#publish_btn').button('loading');
//            },
//            complete: function(xhr) {
//                var json = $.parseJSON(xhr.responseText);
//                if (json.status === "success") {
//                    location.reload();
//                } else {
//                    $('#publish_btn').button('reset');
//                    alert('internal service error');
//                }
//            }
//        });
    });
</script>

<style type="text/css">
    .btn_upload{
        position: relative;
    }

    #new_slide, #new_cover{
        position: absolute;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }

    #cover_txt_holder{
        position: absolute;
        z-index: 0;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        display: block;
        height: 21px;
        color: #999;
        text-align: center;
    }

    .upload_cover_prt{
        width: 150px; 
        height: 150px; 
        border-radius: 3px; 
        border:1px solid #666; 
        background-color: #efefef; 
        position: relative;
        cursor: pointer;
        background-position: center;
        background-size: 100% auto;
        background-repeat: no-repeat;
    }

    .upload_cover_prt:hover{
        opacity: 0.7;
    }
</style>