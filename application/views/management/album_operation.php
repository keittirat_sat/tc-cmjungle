<div class="container">
    <legend><h1><?php echo ($album->album_name) ? "Edit: " . $album->album_name : "Add new" ?></h1></legend>
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title"><i class="glyphicon glyphicon-info-sign"></i> Album information</span>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo site_url('api/update_gallery'); ?>" id="update_gallery_form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Album name: </label>
                            <div class="col-sm-10">
                                <input type="text" value="<?php echo $album->album_name; ?>" name="album_name" class="form-control" placeholder="What is album name ?" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Album detail: </label>
                            <div class="col-sm-10">
                                <textarea rows="6" class="form-control" name="album_detail" placeholder="Write some detail of this album here"><?php echo $album->album_detail; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <button type="button" class="btn btn-danger" id="upload_panel_btn"><i class="glyphicon glyphicon-upload"></i> Upload photo</button>
                            </div>
                        </div>
                        <input type="hidden" name="album_id" value="<?php echo $album->album_id; ?>">
                        <input type="hidden" name="album_approve" value="<?php echo $album->album_approve; ?>">
                        <input type="submit" id="update_album" style="display: none;">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="panel-title"><i class="glyphicon glyphicon-pushpin"></i> Publish</span>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label txt_left">Mode: </label>
                            <div class="col-sm-9">
                                <select class="form-control" name="album_approve" id="mock_approve">
                                    <option value="<?php echo PUBLISH_ALBUM; ?>" <?php echo ($album->album_approve == PUBLISH_ALBUM) ? "selected" : "" ?>>Publish</option>
                                    <option value="<?php echo PRIVATE_ALBUM; ?>" <?php echo ($album->album_approve == PRIVATE_ALBUM) ? "selected" : "" ?>>Private</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Create: </label>
                            <div class="col-sm-9 control-label"><?php echo date('d F Y H:i', $album->album_date); ?></div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-sm-offset-3 col-sm-9 control-label">
                                <button class="btn btn-primary btn-sm" data-loading-text="Updating..." id="update_info"><i class="glyphicon glyphicon-ok"></i> Execute</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="upload_panel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-cog"></i> Upload panel</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#this_album" data-toggle="tab" id="panel_image_holder"><i class="glyphicon glyphicon-picture"></i> Image in this album</a></li>
                            <li><a href="#upload_tab" data-toggle="tab"><i class="glyphicon glyphicon-upload"></i> Upload to this album</a></li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div class="tab-pane active" id="this_album">
                                <div class="row">
                                    <div class="col-md-8" style="overflow-y: auto; height: 316px; border-right: 1px solid #ccc;" id="image_holder_place">   
                                        <?php foreach ($image as $img): ?>
                                            <?php $url = site_url("uploads/{$img['album_id']}/mini_{$img['img_name']}"); ?>
                                            <div class="col-xs-6 col-md-4 holder_frame" style="margin-bottom: 20px;" id="img_<?php echo $img['img_id']; ?>" data-id="<?php echo $img['img_id']; ?>">
                                                <a href="#" class="thumbnail">
                                                    <img data-src="holder.js/100%x180" src="<?php echo $url; ?>">
                                                </a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="col-md-4">
                                        <p>
                                            <a href="#" class="thumbnail" id="holder_del">
                                                <img src="<?php echo image_asset_url("contactus_logo.png") ?>">
                                            </a>
                                        </p>
                                        <p class="txt_center">
                                            <input type="hidden" id="del_img_id">
                                            <button class="btn btn-danger btn-sm" id="del_btn"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="upload_tab">
                                <div style="height: 316px;" id="panel_for_upload">
                                    <button class="btn btn-danger btn-lg" type="button" id="cover_upload_btn">
                                        <i class="glyphicon glyphicon-file"></i> Browse file
                                        <input type="file" name="upload_btn[]" multiple="multiple" id="upload_btn_file">
                                    </button>
                                </div>
                                <div  style="height: 316px; overflow-y: auto;" id="panel_progress_upload">
                                    <ul class="all_progress">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function() {
        var idx_generate = 0;

        $('#del_btn').hide();

        $('#upload_panel_btn').click(function() {
            $('#upload_panel').modal();
        });

        $('#upload_btn_file').change(function() {
            var myupload = this.files;
            success_upload = 0;
            $('#panel_for_upload').hide();
            $('#panel_progress_upload').show();
            for (var index = 0; index < myupload.length; index++) {
                var each_file = myupload[index];
                var id_ref = "bar_progress_" + idx_generate;
                idx_generate++;
                var each_element = '<li id="' + id_ref + '">';
                each_element += '<ul class="progress_each_file">';
                each_element += '<li>' + each_file.name + '</li>';
                each_element += '<li>';
                each_element += '<div class="progress progress-striped active">';
                each_element += '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">';
                each_element += '<span>0% Complete</span>';
                each_element += '</div>';
                each_element += '</div>';
                each_element += '</li>';
                each_element += '</ul>';
                each_element += '</li>';
                $('#panel_progress_upload ul.all_progress').append(each_element);

                var upload_form = new FormData();
                upload_form.append('img_upload', each_file);

                $.ajax({
                    url: '<?php echo site_url("api/upload_gallery?album_id={$album->album_id}"); ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.each_file = each_file;
                            xhr.upload.each_element = each_element;
                            xhr.upload.ref_id = id_ref;
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                                $('#' + this.ref_id).find('span').text(percent + '% Complete');
                                $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                                if (percent == 100) {
                                    $('#' + this.ref_id).fadeOut(500, function() {
                                        $(this).remove();
                                    });
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(e) {
//                        console.log(e);
                        var json = $.parseJSON(e);
                        if (json.status === "success") {
                            console.log('Success', json);
                            var frame = '<div class="col-xs-6 col-md-4 holder_frame" style="margin-bottom: 20px;" id="img_' + json.img_id + '" data-id="' + json.img_id + '">';
                            frame += '<a href="#" class="thumbnail">';
                            frame += '<img data-src="holder.js/100%x180" src="' + json.mini_url + '">';
                            frame += '</a>';
                            frame += '</div>';
                            $('#image_holder_place').append(frame);
                        } else {
                            console.log('Upload fail', e);
                        }
                        success_upload++;
                        if (success_upload == myupload.length) {
                            $('#panel_progress_upload').hide();
                            $('#panel_for_upload').show();
                            $('#panel_image_holder').trigger('click');
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });

        $('#mock_approve').change(function() {
            $('[name=album_approve]').val($('#mock_approve').val());
        });

        $('#update_info').click(function() {
            $('#update_album').trigger('click');
        });

        $('#update_gallery_form').ajaxForm({
            beforeSend: function() {
                $('#update_info').button('loading');
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.href = json.url;
                } else {
                    $('#update_info').button('reset');
                    alert('System cannot update album');
                }
            }
        });

        var ori_temp_img;

        $('.holder_frame').live({
            click: function() {
                var img_id = $(this).attr('data-id');
                ori_temp_img = $('#holder_del').find('img').attr('src');
                $('#del_img_id').val(img_id);
                $('#del_btn').show();
                $('#holder_del').find('img').attr('src', $(this).find('img').attr('src'));
            }
        });

        $('#del_btn').click(function() {
            var img_id = $('#del_img_id').val();
            $.post('<?php echo site_url('api/del_img'); ?>', {'img_id': img_id}, function(res) {
                if (res.status === 'success') {
                    $('#img_' + img_id).fadeOut(400);
                    $('#holder_del').find('img').attr('src', ori_temp_img);
                    $('#del_btn').hide();
                } else {
                    alert('Your request cannot operated');
                }
            }, 'json');
        });
    });
</script>

<style type="text/css">
    .modal-dialog{
        width: 950px;
    }

    .modal-body{
        height: 356px;
    }

    #cover_upload_btn{
        width: 145px;
        height: 47px;
        position: absolute;
        overflow: hidden;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        margin: auto;
    }

    #cover_upload_btn input[type=file]{
        position: absolute;
        z-index: 9;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        opacity: 0;
    }

    #panel_progress_upload{
        display: none;
    }

    ul.all_progress{
        margin: 0px; 
        padding: 0px;
    }

    ul.all_progress li{
        list-style-type: none;
        border-top: 1px solid #999;
    }

    ul.all_progress li:first-of-type{
        border-top-color: transparent;
    }

    ul.all_progress ul.progress_each_file{
        margin: 0px;
        padding: 10px;
        background-color: #efefef;
    }

    ul.all_progress ul.progress_each_file li{
        border-color: transparent;
    }
</style>