<div class="container">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="panel-title"><i class="glyphicon glyphicon-picture"></i> Add new slide</span>
                    </div>
                    <div class="panel-body">
                        <div class="slide_holder txt_center">
                            <h4>Picture dimension: <?php echo slide_width . "x" . slide_height; ?></h4>
                            <div class="progress progress-striped active">
                                <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                    <span>0% Complete</span>
                                </div>
                            </div>
                            <button class='btn btn-info btn-lg btn_upload' data-loading-text='Uploading...'>
                                <i class='glyphicon glyphicon-upload'></i> Upload
                                <input type='file' id='new_slide'>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <?php foreach ($slide as $index => $each_slide): ?>        
            <div class="row" id='img_<?php echo $each_slide['img_id']; ?>'>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title"><i class="glyphicon glyphicon-cog"></i> <?php echo "Slide number: #" . ($index + 1); ?></span>
                        </div>
                        <div class="panel-body">
                            <div class="slide_holder txt_center">
                                <img src='<?php echo $each_slide['url']; ?>' class='img-responsive'>
                            </div>
                        </div>
                        <div class='panel-footer txt_right'>
                            <button class='btn btn-danger btn-sm del_btn' data-loading-text='Deleting...' data-img-id='<?php echo $each_slide['img_id']; ?>'><i class='glyphicon glyphicon-trash'></i> Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </section>
</div>
<style type="text/css">
    .btn_upload{
        position: relative;
    }

    #new_slide{
        position: absolute;
        top: 0;
        left:0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
</style>
<script type='text/javascript'>
    $(function() {
        $('#new_slide').change(function() {
            var file = this.files[0];
            var slideForm = new FormData;
            slideForm.append('img_upload', file);
            $.ajax({
                url: '<?php echo site_url("api/upload_gallery?album_id=" . SLIDE_IMAGE); ?>',
                type: 'POST',
                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) { // Check if upload property exists
                        xhr.upload.addEventListener('progress', function(e) {
                            var percent = parseInt(e.loaded / e.total * 100);
                            $(".progress-bar").width(percent + '%');
                            $(".progress-bar").find('span').text(percent + '% Complete');
                            $(".progress-bar").attr('aria-valuenow', percent);
                            if (percent == 100) {
                                $('#' + this.ref_id).fadeOut(500, function() {
                                    $(this).remove();
                                });
                            }
                        }, false);
                    }
                    return xhr;
                },
                success: function(e) {
//                        console.log(e);
                    var json = $.parseJSON(e);
                    if (json.status === "success") {
                        location.reload();
                    } else {
                        console.log(e);
                        $('#new_slide').button('reset');
                        alert('Upload fail');
                    }
                },
                data: slideForm,
                cache: false,
                contentType: false,
                processData: false
            });
        });

        $('.del_btn').click(function() {
            var img_id = $(this).attr('data-img-id');
            var btn = $(this);
            $(btn).button('loading');
            $.post('<?php echo site_url('api/del_img'); ?>', {img_id: img_id}, function(res) {
                if (res.status == "success") {
                    $('#img_' + img_id).fadeOut(400);
                } else {
                    $(btn).button('reset');
                    alert('Cannot delete slide');
                }
            }, 'json');
        });
    });
</script>