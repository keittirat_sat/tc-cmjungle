<div class="container">
    <div class="row">
        <legend>
            <h1>
                <?php echo $title ?>
                <a href="<?php echo site_url('management/new_guest_album'); ?>" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> New Album</a>
            </h1>
        </legend>
    </div>

    <div class="row">

        <?php if (count($all_album) > 0): ?>

            <?php foreach ($all_album as $each_album): ?>
        
                <div class="col-md-2 col-sm-4">
                    <div class="thumbnail album_popover" style="position: relative; margin-bottom: 30px;" data-toggle="popover" title="<?php echo $each_album->name; ?>" data-content="<?php echo $each_album->detail != "" ? $each_album->detail : "album_detail" ?>">                            
                        <img class="img-responsive" src="<?php echo $each_album->gallery_picture->thumb ?>">
                        <div class="caption">
                            <p><b><?php echo $each_album->name; ?></b></p>
                            <p>Create: <?php echo $each_album->created_at; ?></p>
                            <?php $exp = strtotime($each_album->created_at) + 2592000; ?>
                            <p>Expire: <?php echo date('Y-m-d H:i:s', $exp); ?></p>
                            <p class="txt_right">
                                <a href="<?php echo site_url("management/new_guest_album?album_id={$each_album->id}"); ?>" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                <a href="<?php echo gallery_api . "del-gallery?gallery_id={$each_album->id}&site_id=".site_id; ?>" class="btn btn-xs btn-default del_btn"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                            </p>
                        </div>
                    </div>
                </div>
        
            <?php endforeach; ?>
        <?php else: ?>
            <h3 class="text-center">Not found any album</h3>
        <?php endif; ?>
    </div>
</div>

<style>
    .hover_element{
        display: inline-block;
        width: 86px;
        height: 22px;
        text-align: center;
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        margin: auto;
    }
</style>

<script>
    $(function () {
        $('.album_popover').popover({
            'trigger': 'hover'
        });

        $('.del_btn').click(function () {
            var href = $(this).attr("href");
            var current_block = $(this).parents(".col-md-2.col-sm-4");
            var ele = $(this)
            if (confirm("Please confirm before delete this album")) {
                $(current_block).animate({opacity: '0.7'});
                $.get(href, function (res) {
                    if (res.code == 200) {
                        $(current_block).fadeOut(300, function () {
                            $(ele).remove();
                        });
                    } else {
                        alert("Cannot process request");
                    }
                }, "json");
            }
            return false;
        });
    });
</script>