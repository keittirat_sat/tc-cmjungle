<?php if (count($post)): ?>
    <?php foreach ($post as $each_post): ?>
        <div class="row" id="post_block_<?php echo $each_post->post_id; ?>">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title"><i class="glyphicon glyphicon-paperclip"></i> <?php echo $each_post->post_title; ?></span>
                    </div>
                    <div class="panel-body">
                        <?php $str = strip_tags($each_post->post_detail); ?>
                        <?php echo (trim($str)) ? mb_substr($str, 0, 270, 'utf-8') . "&hellip;" : "<i>-- No Text Available --</i>"; ?>
                    </div>
                    <div class="panel-footer txt_right">
                        <a href="<?php echo site_url('management/post_operation?post_id=' . $each_post->post_id); ?>" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                        <button class="btn btn-danger btn-xs del_btn" data-loading-text="Deleting&hellip;" data-post_id="<?php echo $each_post->post_id; ?>"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="jumbotron">
        <h2>No post found :(</h2>
        <p>Want to add new post ? <a href="<?php echo site_url('management/post_operation'); ?>" class="btn btn-danger btn-sm">Click here</a></p>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(function() {
        $('.del_btn').click(function() {
            var post_id = $(this).attr('data-post_id');
            var btn = $(this);
            $(btn).button('loading');
            $.post('<?php echo site_url('api/del_post'); ?>', {post_id: post_id}, function(res) {
                if (res.status === "success") {
                    $('#post_block_' + post_id).fadeOut(500);
                } else {
                    $(btn).button('reset');
                    alert('Internal service error');
                }
            }, 'json');
        });
    });
</script>