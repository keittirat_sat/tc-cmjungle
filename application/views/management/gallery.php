<div class='container'>
    <legend><h1>Gallery Management <a href="<?php echo site_url('management/album_operation'); ?>" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> New Album</a></h1></legend> 
    <div class='row'>
        <div class='col-md-3'>
            <ul class="nav nav-pills nav-stacked">
                <li <?php echo ($mode == PUBLISH_ALBUM) ? "class='active'" : ""; ?>>
                    <a href="?mode=<?php echo PUBLISH_ALBUM ?>">
                        <span class="badge pull-right"><?php echo $num_album; ?></span>
                        Publish album
                    </a>
                </li>
                <li <?php echo ($mode == PRIVATE_ALBUM) ? "class='active'" : ""; ?>>
                    <a href="?mode=<?php echo PRIVATE_ALBUM ?>">
                        <span class="badge pull-right"><?php echo $num_private_album; ?></span>
                        Private album
                    </a>
                </li>
            </ul>
        </div>
        <div class='col-md-9'>
            <?php foreach ($all_album as $index => $each_album): ?>
                <div class="col-sm-6 col-md-4" style='margin-bottom: 20px;' id='album_holder_<?php echo $each_album['album_id']; ?>'>
                    <div class="thumbnail">
                        <?php if (count($each_album['image'])): ?>
                            <?php $thumbnail = $each_album['image']; ?>
                            <?php $thumbnail = $thumbnail[0]; ?>
                            <?php $img = site_url('uploads/' . $each_album['album_id'] . "/mini_" . $thumbnail->img_name); ?>
                        <?php else: ?>
                            <?php $img = image_asset_url("contactus_mappin.png"); ?>
                        <?php endif; ?>
                        <img alt="<?php echo $each_album['album_name']; ?>" src='<?php echo $img; ?>'>
                        <div class="caption">
                            <h4><?php echo $each_album['album_name']; ?></h4>
                            <p><?php echo ($each_album['album_detail']) ? $each_album['album_detail'] : "<i>No album detail</i>"; ?></p>
                            <p class='txt_right'>
                                <a href="<?php echo site_url('management/album_operation?album_id=' . $each_album['album_id']); ?>" class="btn btn-primary btn-sm" role="button"><i class='glyphicon glyphicon-edit'></i> Edit</a> 
                                <button class="btn btn-default btn-sm del_btn" role="button" data-id='<?php echo $each_album['album_id']; ?>'><i class='glyphicon glyphicon-trash'></i> Delete</button>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.del_btn').click(function() {
            if (confirm('Do you want to delete this album ?')) {
                var album_id = $(this).attr('data-id');
                $.post('<?php echo site_url('api/del_album'); ?>', {album_id: album_id}, function(res) {

                    if (res.status === "success") {
                        $('#album_holder_' + album_id).fadeOut(500);
                    } else {
                        alert("System internal error");
                    }
                }, 'json');
            }
        });
    });
</script>