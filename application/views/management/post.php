<div class='container'>
    <div class="row">
        <div class='col-md-3'>
            <ul class="nav nav-pills nav-stacked">
                <li <?php echo (site_url('management/post') == current_url()) ? $post_status == PUBLISH_POST ? "class='active'" : ""  : ""; ?>><a href='<?php echo site_url('management/post'); ?>'>Publish</a></li>
                <li <?php echo (site_url('management/post') == current_url()) ? $post_status == PRIVATE_POST ? "class='active'" : ""  : ""; ?>><a href='<?php echo site_url('management/post?post_status=' . PRIVATE_POST); ?>'>Private</a></li>
                <li <?php echo (site_url('management/post_operation') == current_url()) ? "class='active'" : ""; ?>><a href='<?php echo site_url('management/post_operation'); ?>'>New post</a></li>
            </ul>
        </div>
        <div class='col-md-9'>
            <?php if (empty($post_id)): ?>
                <?php include 'all_post.php'; ?>
            <?php else: ?>
                <?php include 'edit_post.php'; ?>
            <?php endif; ?>
        </div>
    </div>
</div>