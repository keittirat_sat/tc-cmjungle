<div class="container">
    <section id="testimonial_form">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <span class="panel-title"><i class="glyphicon glyphicon-retweet"></i> Add new pin </span>
                    </div>
                    <div class="panel-body">
                        <?php $folder = date('U'); ?>
                        <div class="row">
                            <div class="col-md-4">
                                <h4><i class="glyphicon glyphicon-upload"></i> Upload reference image</h4>
                                <p>
                                    <a href="#" class="thumbnail">
                                        <img src="<?php echo image_asset_url("header_logo.png"); ?>" class="img-responsive" id="img_pin_<?php echo $folder; ?>">
                                    </a>
                                </p>
                                <span id="progress_bar_<?php echo $folder; ?>"></span>
                                <p class="txt_center">
                                    <button class="btn btn-info btn-xs upload_img" data-loading-text="Uploading...">
                                        <i class="glyphicon glyphicon-upload"></i> Upload
                                        <input type="file" name="upload_ref" data-folder="<?php echo $folder; ?>">
                                    </button>
                                    <button class="btn btn-danger btn-xs remove_btn" data-folder="<?php echo $folder; ?>" id="del_btn_<?php echo $folder; ?>"><i class="glyphicon glyphicon-trash"></i> Delete image</button>
                                </p>
                            </div>
                            <div class="col-md-8">
                                <form class="form-horizontal upinfo" method="post" action="<?php echo site_url('api/uploadpin'); ?>" data-form-type="upload">
                                    <h4><i class="glyphicon glyphicon-info-sign"></i> Review information</h4>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Title</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="link_title" class="form-control" required="required" placeholder="Review title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Author</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="link_author" class="form-control" placeholder="Author">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Link</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="link" class="form-control" required="required"  placeholder="Url">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Detail</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" rows="6" name="link_detail" placeholder="Just a short detail of this review"></textarea>
                                        </div>
                                    </div>
                                    <input type="hidden" name="folder" value="<?php echo $folder; ?>">
                                    <input type="hidden" name="filename"  id="filename_<?php echo $folder; ?>" value="0">
                                    <input type="submit" id="submit_form_<?php echo $folder; ?>" style="display: none;">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer txt_right">
                        <button class="btn btn-sm btn-default btn-sm" type="reset"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
                        <button class="btn btn-sm btn-primary btn-sm submit_form" type="submit" data-loading-text="Adding..." data-submit-id="submit_form_<?php echo $folder; ?>" id="submit_form_btn_<?php echo $folder; ?>"><i class="glyphicon glyphicon-plus"></i> Add</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonial">
        <?php foreach ($all_pin as $pin): ?>
            <div class="row" id="each_review_<?php echo $pin->link_id; ?>">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title"><i class="glyphicon glyphicon-user"></i> <?php echo $pin->link_title ?></span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4><i class="glyphicon glyphicon-upload"></i> Upload reference image</h4>
                                    <p>
                                        <a href="<?php echo site_url("uploads/{$pin->folder}/{$pin->filename}"); ?>" class="thumbnail colorbox" style="max-height: 300px; overflow: hidden;">
                                            <?php if ($pin->filename == '0'): ?>
                                                <img src="<?php echo image_asset_url("header_logo.png"); ?>" class="img-responsive" id="img_pin_<?php echo $pin->folder; ?>">
                                            <?php else: ?>
                                                <img src="<?php echo site_url("uploads/{$pin->folder}/{$pin->filename}"); ?>" class="img-responsive" id="img_pin_<?php echo $pin->folder; ?>">
                                            <?php endif; ?>
                                        </a>
                                    </p>
                                    <span id="progress_bar_<?php echo $pin->folder; ?>"></span>
                                    <p class="txt_center">
                                        <button class="btn btn-info btn-xs upload_img" data-loading-text="Uploading...">
                                            <i class="glyphicon glyphicon-upload"></i> Upload
                                            <input type="file" name="upload_ref" data-pin-id="<?php echo $pin->link_id; ?>" data-folder="<?php echo $pin->folder; ?>">
                                        </button>
                                        <button class="btn btn-danger btn-xs remove_btn" data-folder="<?php echo $pin->folder; ?>" id="del_btn_<?php echo $pin->folder; ?>"><i class="glyphicon glyphicon-trash"></i> Delete image</button>
                                    </p>
                                </div>
                                <div class="col-md-8">
                                    <form class="form-horizontal upinfo" method="post" action="<?php echo site_url('api/updatepin'); ?>" data-form-type="update">
                                        <h4><i class="glyphicon glyphicon-info-sign"></i> Review information</h4>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Title</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="link_title" class="form-control" required="required" placeholder="Review title" value="<?php echo $pin->link_title; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Author</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="link_author" class="form-control" placeholder="Author" value="<?php echo $pin->link_author; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Link</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="link" class="form-control" required="required"  placeholder="Url" value="<?php echo $pin->link; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Detail</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" rows="6" name="link_detail" placeholder="Just a short detail of this review"><?php echo $pin->link_detail; ?></textarea>
                                            </div>
                                        </div>
                                        <input type="hidden" name="folder" value="<?php echo $pin->folder; ?>">
                                        <input type="hidden" name="link_id" value="<?php echo $pin->link_id; ?>">
                                        <input type="hidden" name="filename" value="<?php echo $pin->filename; ?>" id="filename_<?php echo $pin->folder; ?>">
                                        <input type="submit" id="submit_form_<?php echo $pin->folder; ?>" style="display: none;">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer txt_right">
                            <button class="btn btn-sm btn-primary submit_form" type="submit" data-loading-text="Updating..." data-submit-id="submit_form_<?php echo $pin->folder; ?>" id="submit_form_btn_<?php echo $pin->folder; ?>"><i class="glyphicon glyphicon-check"></i> Update</button>
                            <button class="btn btn-sm btn-default" type="reset"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
                            <button class="btn btn-sm btn-danger del_review" type="button" data-loading-text="Deleting..." data-link-id="<?php echo $pin->link_id; ?>" data-folder="<?php echo $pin->folder; ?>"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </section>
</div>

<style type="text/css">
    .upload_img{
        position: relative;
        overflow: hidden;
    }

    .upload_img input[type=file]{
        position: absolute;
        z-index: 99;
        top: 0;
        left: 0;
        opacity: 0;
    }
</style>

<script type="text/javascript">
    $(function() {
        $('[name=upload_ref]').change(function() {
            var folder = $(this).attr('data-folder');
            var file = this.files[0];
            var form = new FormData;
            form.append('upload_btn', file);
            var id_ref = 'progress_' + folder;
            var each_element = '<div class="progress" id="' + id_ref + '">';
            each_element += '<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">';
            each_element += '<span>0% Complete</span>';
            each_element += '</div>';
            each_element += '</div>';
            $('#progress_bar_' + folder).html(each_element);
            $.ajax({
                url: '<?php echo site_url('api/uploadhandle?'); ?>folder=' + folder,
                type: 'POST',
                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) { // Check if upload property exists
                        xhr.upload.each_file = file;
                        xhr.upload.each_element = each_element;
                        xhr.upload.ref_id = id_ref;
                        xhr.upload.addEventListener('progress', function(e) {
                            var percent = parseInt(e.loaded / e.total * 100);

                            $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                            $('#' + this.ref_id).find('span').text(percent + '% Complete');
                            $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                            if (percent == 100) {
                                $('#' + this.ref_id).fadeOut(500, function() {
                                    $(this).remove();
                                });
                            }
                        }, false);
                    }
                    return xhr;
                },
                success: function(e) {
                    var json = $.parseJSON(e);
                    console.log(json);
                    $('#filename_' + json.folder).val(json.response.file_name);
                    $('#img_pin_' + json.folder).attr('src', json.url);
                    $('#del_btn_' + json.folder).show();
                },
                data: form,
                cache: false,
                contentType: false,
                processData: false
            });
        });

        $('a.thumbnail').mouseenter(function() {
            $(this).css({'max-height': '3000px', 'overflow': 'visible'});
        }).mouseleave(function() {
            $(this).css({'max-height': '300px', 'overflow': 'hidden'});
        });

        $('.submit_form').click(function() {
            var target = $(this).attr('data-submit-id');
            $('#' + target).trigger('click');
        });

        $('.upinfo').each(function(id, ele) {
            var folder = $(ele).find('[name=folder]').val();
            var submit = $('#submit_form_btn_' + folder);
            var filename = $(ele).find('[name=filename]').val();
            if (filename === "0") {
                $('#del_btn_' + folder).hide();
            }
            $(ele).ajaxForm({
                beforeSend: function() {
                    $(submit).button('loading');
                },
                complete: function(xhr) {
                    console.log(xhr);
                    var json = $.parseJSON(xhr.responseText);
                    if (json.status === "success") {
                        location.reload();
                    } else {
                        alert('Cannot procress your request');
                        $(submit).button('reset');
                    }
                }
            });
        });

        $('.del_review').click(function() {
            var link_id = $(this).attr('data-link-id');
            var btn = $(this);
            $(btn).button('loading');
            var target = 'each_review_' + link_id;
            $.post('<?php echo site_url('api/del_pin'); ?>', {pin_id: link_id}, function(res) {
                if (res.status === "success") {
                    $('#' + target).fadeOut(400);
                } else {
                    alert('Cannot process your request');
                    $(btn).button('reset');
                }
            }, 'json');
        });

        $('.remove_btn').click(function() {
            var folder = $(this).attr('data-folder');
            var target = '#img_pin_' + folder;
            $(this).hide();
            $(target).attr('src', '<?php echo site_url('assets/image/header_logo.png'); ?>');
            $('#filename_' + folder).val('0');
        });

    });
</script>