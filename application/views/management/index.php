<div class="login_panel">
    <div class="panel panel-default">
        <div class="panel-heading">
            <img src="<?php echo image_asset_url("About_banner.png") ?>" class="img-responsive">            
        </div>
        <div class="panel-body">
            <div class="alert alert-success">Welcome to TourCMS</div>
            <form class="form-horizontal" id="login_form">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class=" form-control" name="username" required="required" placeholder="Username here">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" class=" form-control" name="password" required="required" placeholder="Password here">
                    </div>
                </div>
                <input type="submit" id="submit_btn_hide">
            </form>
        </div>
        <div class="panel-footer txt_right">
            <button class="btn btn-default" id="reset_btn"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
            <button class="btn btn-primary" id="submit_btn" data-loading-text="Loging..."><i class="glyphicon glyphicon-log-in"></i> Login</button>
        </div>
    </div>

    <p class="txt_center">Powered by <a href="http://www.trycatch.in.th" target="_blank">TryCatch</a></p>
</div>

<style type="text/css">
    .login_panel{
        width: 380px;
        height: 423px;
        position: absolute;
        top: -50px;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
    }
    #submit_btn_hide{
        display: none;
    }
</style>

<script type="text/javascript">
    $(function() {
        $('#submit_btn').click(function() {
            $('#submit_btn_hide').trigger('click');
        });

        $('#reset_btn').click(function() {
            $('#submit_btn').button('reset');
            $('#login_form').trigger('reset');
        });

        $('#login_form').attr({'action': '<?php echo site_url('api/login'); ?>', 'method': 'post'});

        $('#login_form').ajaxForm({
            beforeSend: function() {
                $('#submit_btn').button('loading');
            },
            complete: function(res) {
                console.log(res.responseText);
                var json = $.parseJSON(res.responseText);
                if (json.status === "success") {
                    $('#submit_btn').text('Loged !!!');
                    setTimeout(function() {
                        location.href = json.url;
                    }, 1000);
                } else {
                    $('#submit_btn').button('reset');
                    $('.alert').removeClass('alert-success').addClass('alert-danger').text('Username/Password is not correct');
                }
            }
        });
    });
</script>