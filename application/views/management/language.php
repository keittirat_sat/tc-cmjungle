<div class='container'>
    <div class="row">
        <div class='col-md-3'>
            <ul class="nav nav-pills nav-stacked">
                <li <?php echo (site_url('management/language') == current_url()) ? "class='active'" : ""; ?>><a href='<?php echo site_url('management/language'); ?>'>All Languages</a></li>
                <li <?php echo (site_url('management/language_operation') == current_url()) ? "class='active'" : ""; ?>><a href='<?php echo site_url('management/language_operation'); ?>'>New Language</a></li>
            </ul>
        </div>
        <div class='col-md-9'>
            <?php if (empty($lang_id)) : ?>
                <?php include 'all_language.php'; ?>
            <?php else: ?>
                <?php include 'edit_language.php'; ?>
            <?php endif; ?>
        </div>
    </div>
</div>