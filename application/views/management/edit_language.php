<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <form action="<?php echo site_url('api/update_lang'); ?>" method="post" class='form-horizontal' id='update_lang'>
                <div class="panel-heading">
                    <span class="panel-title"><i class="glyphicon glyphicon-globe"></i> Languages information</span>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="lang_name" class="form-control" placeholder="Language's name" value='<?php echo $lang->lang_name; ?>'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Post unavailable</label>
                        <div class="col-sm-9">
                            <textarea name="lang_info" class="form-control" rows="5" placeholder="Display when post unavailable"><?php echo $lang->lang_info; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="panel-footer txt_right">
                    <button class="btn btn-primary btn-sm" type='submit' id='submit_btn' data-loading-text='Uploading...'><i class="glyphicon glyphicon-upload"></i> Upload</button>
                    <?php if ($lang->lang_id != 1): ?>
                        <button class="btn btn-danger btn-sm" type='button' data-loading-text='Deleting...' id='del_btn'><i class="glyphicon glyphicon-trash"></i> Delete</button>
                    <?php endif; ?>
                </div>
                <input type='hidden' value='<?php echo $lang->lang_id; ?>' name='lang_id'>
            </form>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function() {
        $('#update_lang').ajaxForm({
            beforeSend: function() {
                $('#submit_btn').button('loading');
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    location.href = '<?php echo site_url('management/language'); ?>';
                } else {
                    $('#submit_btn').button('reset');
                    alert('Cannot update record');
                }
            }
        });
<?php if ($lang->lang_id != 1): ?>
            $('#del_btn').click(function() {
                var del_btn = $(this);
                $(del_btn).button('loading');
                $.post('<?php echo site_url('api/del_lang'); ?>', {lang_id: $('[name=lang_id]').val()}, function(res) {
                    if (res.status === "success") {
                        location.href = '<?php echo site_url('management/language'); ?>';
                    } else {
                        $(del_btn).button('reset');
                        alert('Cannot update record');
                    }
                }, 'json');
            });
<?php endif; ?>
    });
</script>