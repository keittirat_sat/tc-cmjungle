<div class="container">
    <legend><h1><?php echo ($album->name) ? "Edit Guest: " . $album->name : "Add new guest album" ?></h1></legend>
    <div class="row">
        <div class="col-xs-9">

            <!--Album information-->
            <div class="panel panel-default">
                <div class="panel-heading"><span class="panel-title"><i class="glyphicon glyphicon-info-sign"></i> Album information</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="<?php echo gallery_api.'update-album?site_id='.site_id; ?>" id="update_gallery_form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Album name: </label>
                            <div class="col-sm-10">
                                <input type="text" value="<?php echo $album->name; ?>" name="name" class="form-control" placeholder="What is album name ?" required="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Album detail: </label>
                            <div class="col-sm-10">
                                <textarea rows="6" class="form-control" name="detail" placeholder="Write some detail of this album here"><?php echo $album->detail; ?></textarea>
                            </div>
                        </div>

                        <!--Hidden field-->
                        <input type="hidden" name="gallery_id" value="<?php echo $album->id; ?>">
                        <input type="hidden" name="status" value="waiting">
                        <input type="submit" id="update_album" style="display: none;">
                        <!--Hidden field-->
                    </form>
                </div>
            </div><!--Album information-->

            <!--Progress-->
            <div class="panel panel-default" id="progress_section">
                <div class="panel-heading"><i class="glyphicon glyphicon-asterisk"></i> Upload Progress</div>
                <div class="panel-body" id="panel_progress_upload">
                    <ul class="all_progress"></ul>
                </div>
            </div><!--Progress-->

            <!--Album image-->
            <div class="panel panel-default">
                <div class="panel-heading"><span class="panel-title">
                        <div class="row">
                            <div class="col-xs-6">
                                <i class="glyphicon glyphicon-picture"></i> Photo
                            </div>
                            <div class="col-xs-6 txt_right">
                                <button class="btn btn-danger btn-xs" id="file_cover_upload" style="position: relative;">
                                    <i class="glyphicon glyphicon-play-circle"></i> New Photo
                                    <input type="file" name="media[]" multiple="multiple" id="upload_btn_file">
                                </button>
                            </div>
                        </div>
                </div>
                <div class="panel-body">
                    <?php $i_row = 0; ?>
                    <?php $regroup[$i_row] = array(); ?>
                    <?php foreach ($image as $index => $each_image): ?>
                        <?php array_push($regroup[$i_row], $each_image) ?>
                        <?php if (($index % 4) == 3): ?>
                            <?php $i_row++; ?>
                            <?php $regroup[$i_row] = array(); ?>
                        <?php endif ?>
                    <?php endforeach; ?>

                    <?php foreach ($regroup as $each_rows): ?>
                        <div class="row" style="margin-bottom: 15px;">
                            <?php foreach ($each_rows as $each_image): ?>
                                <div class="col-xs-3">
                                    <a href="<?php echo gallery_api."del-picture?picture_id={$each_image->id}&site_id=".site_id ?>" class="thumbnail frame_image">
                                        <img src="<?php echo $each_image->thumb ?>" alt="<?php echo $each_image->id ?>">
                                        <button class="btn btn-xs btn-danger">Delete</button>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                    <div id="image_holder_place" class="row"></div>
                </div>
            </div><!--Album image-->

        </div>
        <div class="col-xs-3">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-pushpin"></i> Publish</div>
                <div class="panel-body txt_center">
                    <button class="btn btn-success" id="save_btn" data-loading-text="Saving..."><i class="glyphicon glyphicon-save"></i> Save</button>
                    <button class="btn btn-default" id="reset_btn"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var idx_generate = 0;
        $('#progress_section').fadeOut(0);     

        $('#upload_btn_file').change(function() {
            var myupload = this.files;
            success_upload = 0;
            $('#panel_for_upload').hide();
            $('#panel_progress_upload').show();

            $('#progress_section').fadeIn(400);
            for (var index = 0; index < myupload.length; index++) {
                var each_file = myupload[index];
                var id_ref = "bar_progress_" + idx_generate;
                idx_generate++;
                var each_element = '<li id="' + id_ref + '">';
                each_element += '<ul class="progress_each_file">';
                each_element += '<li>' + each_file.name + '</li>';
                each_element += '<li>';
                each_element += '<div class="progress progress-striped active">';
                each_element += '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">';
                each_element += '<span>0% Complete</span>';
                each_element += '</div>';
                each_element += '</div>';
                each_element += '</li>';
                each_element += '</ul>';
                each_element += '</li>';
                $('#panel_progress_upload ul.all_progress').append(each_element);

                var upload_form = new FormData();
                upload_form.append('media', each_file);
                upload_form.append('gallery_id', <?php echo $album->id; ?>);

                $.ajax({
                    url: '<?php echo site_url("api/relay_gallery") ?>',
                    type: 'POST',
                    xhr: function() {
                        var xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) { // Check if upload property exists
                            xhr.upload.each_file = each_file;
                            xhr.upload.each_element = each_element;
                            xhr.upload.ref_id = id_ref;
                            xhr.upload.addEventListener('progress', function(e) {
                                var percent = parseInt(e.loaded / e.total * 100);
                                $("#" + this.ref_id).find(".progress-bar").width(percent + '%');
                                $('#' + this.ref_id).find('span').text(percent + '% Complete');
                                $('#' + this.ref_id).find('.progress-bar').attr('aria-valuenow', percent);
                                if (percent == 100) {
                                    $('#' + this.ref_id).fadeOut(500, function() {
                                        $(this).remove();
                                    });
                                }
                            }, false);
                        }
                        return xhr;
                    },
                    success: function(json) {
                        json = $.parseJSON(json);
                        console.log(json);
                        if (json.code === 200) {
                            console.log('Success');
                            var frame = '<div class="col-xs-3" id="img_' + json.result.id + '" data-id="' + json.result.id + '">';
                            frame += '<a href="<?php echo gallery_api."del-picture?site_id=".site_id."&picture_id="; ?>' + json.result.id + '" class="thumbnail frame_image">';
                            frame += '<img src="' + json.result.thumb + '">';
                            frame += '<button class="btn btn-xs btn-danger">Delete</button>';
                            frame += '</a>';
                            frame += '</div>';
                            $('#image_holder_place').append(frame);
                        } else {
                            console.log('Upload fail', json);
                        }
                        success_upload++;
                        if (success_upload == myupload.length) {
                            $('#progress_section').fadeOut(400);
                            $('#panel_for_upload').show();
                            $('#panel_image_holder').trigger('click');
                        }
                    },
                    data: upload_form,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

        });

        $('a.frame_image button').css({opacity: 0});
        $('a.frame_image').live({
            mouseenter: function() {
                $(this).find("button").stop().animate({opacity: 1}, 200);
            },
            mouseleave: function() {
                $(this).find("button").stop().animate({opacity: 0}, 200);
            }
        });

        $('a.frame_image').live({
            click: function() {
                var href = $(this).attr("href");
                var this_element = $(this);
                if (confirm("Do you want to delete this image ?")) {
                    $.get(href, function(res) {
                        if (res.code === 200) {
                            $(this_element).fadeOut(300, function() {
                                $(this_element).parents('.col-xs-3').remove();
                            });
                        } else {
                            alert("System Error : Cannot process your request");
                        }
                    }, "json");
                }
                return false;
            }
        });

        $('#update_gallery_form').ajaxForm({
            beforeSend: function() {
                $('#save_btn').button("loading");
            },
            complete: function(xhr) {
                var jsonp = xhr.responseText;
                $('#save_btn').button("reset");
                jsonp = $.parseJSON(jsonp);
                if (jsonp.code === 200) {
                    alert("Update success");
                    location.href = "/management/guestalbum";
                } else {
                    alert("Cannot update post data");
                }
            }
        });

        $('#save_btn').click(function() {
            $('#update_album').trigger("click");
        });
    });
</script>

<style type="text/css">
    #upload_btn_file{
        position: absolute;
        z-index: 9;
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        opacity: 0;
    }

    ul.progress_each_file, ul.all_progress{
        padding-left: 0px;
    }

    .progress_each_file li, .all_progress li{
        list-style: none;
    }

    .frame_image{
        position: relative;
    }

    .frame_image .btn-danger{
        position: absolute;
        width: 47px;
        height: 22px;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        margin: auto;
    }
</style>