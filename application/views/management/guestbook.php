<div class='container'>
    <legend><h1>Guestbook Management</h1></legend>
    <div class='row'>
        <div class='col-md-3'>
            <ul class="nav nav-pills nav-stacked">
                <li <?php echo ($mode == GUESTBOOK_APPROVED) ? "class='active'" : ""; ?>>
                    <a href="?mode=<?php echo GUESTBOOK_APPROVED ?>&page=0">
                        <span class="badge pull-right"><?php echo $num_approve; ?></span>
                        Approved
                    </a>
                </li>
                <li <?php echo ($mode == GUESTBOOK_NOT_APPROVE) ? "class='active'" : ""; ?>>
                    <a href="?mode=<?php echo GUESTBOOK_NOT_APPROVE ?>&page=0">
                        <span class="badge pull-right"><?php echo $num_non_approve; ?></span>
                        Not Approved
                    </a>
                </li>
            </ul>
        </div>
        <div class='col-md-9'>
            <div class='txt_right'>
                <ul class="pagination pagination-sm" style='margin-top: 0px;'>     
                    <?php for ($i = 0; $i < $all_page; $i++): ?>
                        <li <?php echo ($i == $page) ? "class='active'" : "" ?>><a href='?mode=<?php echo $mode ?>&page=<?php echo $i; ?>'><?php echo $i + 1; ?></a></li>
                    <?php endfor; ?>
                </ul>
            </div>

            <?php foreach ($app_guestbook as $index => $each_guestbook): ?>
                <div id='<?php echo "panel_id_" . $each_guestbook->guest_id ?>' class='panel <?php echo ($each_guestbook->guest_approve == GUESTBOOK_APPROVED) ? "panel-success" : "panel-danger"; ?>'>
                    <div class='panel-heading'>
                        <span class='panel-title'><i class='glyphicon glyphicon-user'></i> <?php echo $each_guestbook->guest_name; ?></span>
                        <span class='pull-right'><i class='glyphicon glyphicon-time'></i> <?php echo date('d F Y H:i', $each_guestbook->guest_timestamp); ?>&nbsp;<?php echo ($each_guestbook->guest_approve == GUESTBOOK_NOT_APPROVE) ? "<button class='btn btn-success btn-xs approve_btn' data-id='{$each_guestbook->guest_id}' data-loading-text='Approving...'><i class='glyphicon glyphicon-check'></i> Approve</button>" : ""; ?></span>
                    </div>
                    <div class='panel-body'>
                        <?php echo auto_link($this->typography->auto_typography($each_guestbook->guest_detail, true)); ?>
                    </div>
                    <div class='panel-footer txt_right'> 
                        <?php if (!empty($each_guestbook->guest_email)): ?>
                            <a class='btn btn-info btn-xs' href="mailto:<?php echo trim($each_guestbook->guest_email); ?>"><i class='glyphicon glyphicon-envelope'></i> <?php echo $each_guestbook->guest_email; ?></a>
                        <?php endif; ?>
                        <button class='btn btn-danger btn-xs del_btn' data-id='<?php echo $each_guestbook->guest_id; ?>' data-loading-text='Deletng...'><i class='glyphicon glyphicon-trash'></i> Delete</button>
                    </div>
                </div>
            <?php endforeach; ?>            

            <div class='txt_right'>
                <ul class="pagination pagination-sm">     
                    <?php for ($i = 0; $i < $all_page; $i++): ?>
                        <li <?php echo ($i == $page) ? "class='active'" : "" ?>><a href='?mode=<?php echo $mode ?>&page=<?php echo $i; ?>'><?php echo $i + 1; ?></a></li>
                    <?php endfor; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function() {
        $('.approve_btn').click(function() {
            var post_id = $(this).attr('data-id');
            $(this).button('loading');
            $.post('<?php echo site_url('api/approve_post'); ?>', {post_id: post_id}, function(res) {
                if (res.status === "success") {
                    $('#panel_id_' + post_id).removeClass('panel-danger').addClass('panel-success');
                    setTimeout(function() {
                        $('#panel_id_' + post_id).fadeOut(400);
                    }, 1000);
                } else {
                    alert('System Internal error : #' + post_id);
                }
                $(this).button('reset');
            }, 'json');
        });

        $('.del_btn').click(function() {
            if (confirm('Do you want to delete this comment?')) {
                var post_id = $(this).attr('data-id');
                $(this).button('loading');
                $.post('<?php echo site_url('api/del_guestbook'); ?>', {post_id: post_id}, function(res) {
                    if (res.status === "success") {
                        $('#panel_id_' + post_id).fadeOut(400);
                    } else {
                        alert('System Internal error : #' + post_id);
                    }
                    $(this).button('reset');
                }, 'json');
            }
        });
    });
</script>