<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <!--css section-->
        <?php echo css_asset("bootstrap.min.css"); ?>
        <?php echo (empty($css)) ? "" : $css; ?>
        <!--/css section-->

        <!--js section-->
        <?php echo js_asset("jquery-1.7.min.js"); ?>
        <?php echo js_asset("bootstrap.min.js"); ?>
        <?php echo (empty($js)) ? "" : $js; ?>
        <!--/js section-->
    </head>
    <body>
        <?php echo $contents; ?>
    </body>
</html>
