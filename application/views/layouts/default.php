<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="icon" href="<?php echo image_asset_url(" favicon.ico "); ?>">
    <meta name="description" content="ChiangmaiJungleTrekking.com : We are trekking tour in Chiangmai" />
    <meta name="keywords" content="trekking by toto, chiangmai trekking, hungle trekking, elephat riding" />
    <title>
        <?php echo $title; ?>
    </title>
    <!--css section-->
    <?php echo css_asset( "bootstrap.min.css"); ?>
    <?php echo css_asset( "sweetalert.css"); ?>
    <?php echo (empty($css)) ? "" : $css; ?>
    <!--/css section-->

    <!--js section-->
    <?php echo js_asset( "jquery-1.7.min.js"); ?>
    <?php echo js_asset( "bootstrap.min.js"); ?>
    <?php echo js_asset( "sweetalert.min.js"); ?>
    <script src="https://kit.fontawesome.com/cb30968966.js"></script>
    <?php echo (empty($js)) ? "" : $js; ?>
    <!--/js section-->

    <!--
        <script type="text/javascript">
            $(function(){
                swal({
                    title: "Sorry !!!",
                    text: "Now this website is running in maintenance mode, if you want to contact us for more information please contact directly via `chiangmaijungletrekking@gmail.com`, Please do not use contact form in this website. We apologise for any inconvenience issue",
                    type: "warning"
                }, function(){
                    $('.form-contact').slideUp(2000);
                });
            });
        </script>
-->

</head>

<body>
    <?php if (loged()): ?>
    <style type="text/css">
        body {
            padding-top: 70px;
        }
    </style>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="nav-header hidden-md hidden-sm hidden-xs">
            <a href="<?php echo $nav_url; ?>" class="navbar-brand"><i class="glyphicon glyphicon-tags"></i>&nbsp;&nbsp;Chiangmai Jungle Trekking</a>
        </div>
        <div class="navbar-collapse navbar-right">
            <a class="btn btn-danger navbar-btn" onclick="$(this).button('loading');" data-loading-text='Logging out...' href="<?php echo site_url('api/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a>
        </div>
        <div class="navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo site_url('management/home'); ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                </li>
                <li>
                    <a href="<?php echo site_url('management/post'); ?>"><i class="glyphicon glyphicon-tasks"></i> Trip</a>
                </li>
                <li>
                    <a href="<?php echo site_url('management/slide'); ?>"><i class="glyphicon glyphicon-bullhorn"></i> Slide</a>
                </li>
                <li>
                    <a href="<?php echo site_url('management/review'); ?>"><i class="glyphicon glyphicon-list"></i> Review</a>
                </li>
                <li>
                    <a href="<?php echo site_url('management/guestbook'); ?>"><i class="glyphicon glyphicon-book"></i> Guestbook <span class="badge badge-warning"><?php echo $this->post->get_number_of_guestbook(GUESTBOOK_NOT_APPROVE); ?></span></a>
                </li>
               <li>
                   <a href="<?php echo site_url('management/guestalbum'); ?>"><i class="glyphicon glyphicon-picture"></i> Guest Album</a>
               </li>
                <li>
                    <a href="<?php echo site_url('management/gallery'); ?>"><i class="glyphicon glyphicon-picture"></i> Gallery</a>
                </li>
                <li>
                    <a href="<?php echo site_url('management/language'); ?>"><i class="glyphicon glyphicon-globe"></i> Language</a>
                </li>
            </ul>
        </div>
    </nav>
    <?php endif; ?>
    <?php echo!empty($header) ? $header : ""; ?>
    <?php echo $contents; ?>
    <?php echo!empty($footer) ? $footer : ""; ?>
</body>

</html>