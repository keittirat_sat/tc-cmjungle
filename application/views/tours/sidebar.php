<div class='row hidden-sm hidden-xs'>
    <div class='col-md-12 txt_center'>
        <div class="slide_frame_mini">
            <?php $img = $this->post->get_img_by_type(25); ?>
            <a href="#prev" class="custom_nav custom_nav_prev">prev</a>
            <a href="#next" class="custom_nav custom_nav_next">prev</a>
            <div class="mini_slide">
                <?php foreach ($img as $each_img): ?>
                    <?php if (is_file("./uploads/{$each_img->album_id}/mini_{$each_img->img_name}")): ?>
                        <?php $info = getimagesize("./uploads/{$each_img->album_id}/mini_{$each_img->img_name}"); ?>
                        <?php if ($info[0] > $info[1]): ?>
                            <img src="<?php echo site_url("uploads/{$each_img->album_id}/mini_{$each_img->img_name}"); ?>">
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <a href="<?php echo site_url('tours/gallery'); ?>" class="view_more"></a>
        </div>
    </div>    
</div>

<script type="text/javascript">
    $(function() {
        $('.mini_slide').nivoSlider();

        $('.custom_nav_prev').click(function() {
            $('a.nivo-prevNav').trigger('click');
            return false;
        });

        $('.custom_nav_next').click(function() {
            $('a.nivo-nextNav').trigger('click');
            return false;
        });

        $('a.hover_elephant, a.hover_elephant_album').mouseenter(function() {
            $(this).find("img").stop().animate({opacity: '1'});
        }).mouseleave(function() {
            $(this).find("img").stop().animate({opacity: '0'});
        });
    });
</script>

<!--Guest Section-->
<h1 class='carbon txt_upper txt_center orange'><i>guestbook</i></h1>
<?php foreach ($guestbook as $each_guest): ?>
    <div class='row'>
        <div class='col-md-12'>
            <p class='gray txt_justify txt_ittalic' style='font-size: 12px; text-indent: 30px; margin-bottom: 0px;'><?php echo mb_substr(trim(strip_tags($each_guest->guest_detail)), 0, 300, 'utf-8'); ?>&hellip;</p>
            <p class='txt_right orange txt_ittalic' style='font-size: 12px;'><?php echo $each_guest->guest_name; ?></p>
        </div>
    </div>
<?php endforeach; ?>
<div class='row' style='margin-bottom: 20px'>
    <div class='col-md-12 txt_right'>
        <a href="<?php echo site_url('tours/guestbook'); ?>">
            <?php echo image_asset("btu_readmore.png") ?>
        </a>
    </div>
</div>
<!--/Guest Section-->

<?php if (count($pin)): ?>
    <!--Review Section-->   
    <h1 class='carbon txt_upper txt_center orange'><i>review</i></h1>
    <?php foreach ($pin as $each_pin): ?>
        <div class='row'>
            <div class='col-md-12'>
                <p class='margin-bottom:0px;' class='orange'><b><?php echo $each_pin->link_title; ?></b></p>
                <p class='gray txt_justify txt_ittalic' style='font-size: 12px; text-indent: 30px; margin-bottom: 0px;'><?php echo mb_substr(trim(strip_tags($each_pin->link_detail)), 0, 300, 'utf-8'); ?>&hellip;</p>
                <p class='txt_right orange txt_ittalic' style='font-size: 12px;'><b><?php echo $each_pin->link_author; ?></b> | <a href='<?php echo $each_pin->folder == 0 ? $each_pin->link : site_url('tours/pin_review/' . $each_pin->link_id . '/' . url_title($each_pin->link_title)); ?>' class='orange'>VIEW>></a></p>
            </div>
        </div>
    <?php endforeach; ?>
    <div class='row' style='margin-bottom: 20px'>
        <div class='col-md-12 txt_right'>
            <a href="<?php echo site_url('tours/review'); ?>">
                <?php echo image_asset("btu_readmore.png"); ?>
            </a>
        </div>
    </div>
    <!--/Review Section-->
<?php endif; ?>

<div class='row hidden-sm hidden-xs '>
    <div class='col-md-12 txt_center'>
        <div class="fb-like-box" data-href="<?php echo FB_FANPAGE; ?>" data-height="575" data-colorscheme="dark" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
    </div>
</div>