<div class="container">
    <div class="panel panel-default">
        <div class="panel-body">
            <h2 class="carbon orange txt_center"><?php echo $album->name ?></h2>
            <p class="txt_center"><?php echo $album->detail ?></p>
            <p class="txt_center" style="margin-bottom: 30px;">
                <?php if (is_null($album->zip_url)): ?>
                <button class="btn btn-inverse btn-sm" disabled="disabled">On processing...</button>
                <?php else: ?>
                    <a href="<?php echo $album->zip_url ?>" class="btn btn-sm btn-danger">
                        <i class="glyphicon glyphicon-download-alt"></i>&nbsp;Download All
                    </a>
                <?php endif; ?>
            </p>

            <div class="row">
                <?php foreach ($image as $each_image): ?>
                    <div class="col-md-3 col-sm-4" style="margin-bottom: 20px;">
                        <a href="<?php echo $each_image->url ?>" class="thumbnail">
                            <img src="<?php echo $each_image->thumb ?>">
                        </a>
                    </div>
                <?php endforeach; ?>                    
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id='modal_live' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-body">
      <img src="" alt="" class="img-responsive" id='img_replace' />
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
    $('a.thumbnail').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $('#img_replace').attr('src',url);
        $('#modal_live').modal('show');
    });
});
</script>