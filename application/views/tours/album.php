<div style="margin-top: 25px; " class="container">
    <div class="row">
        <div class="col-md-12">
            <p><?php echo image_asset("Untitled-1_03.png"); ?></p>
            <h1 class="carbon orange"><i><?php echo $album['album_name']; ?></i></h1>
            <?php if (trim($album['album_detail']) != ""): ?>
                <p><?php echo $album['album_detail']; ?></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <?php foreach ($album['image'] as $each_pic): ?>
            <a class="col-lg-3 col-md-4 col-sm-6" data-gallery="multiimages" data-toggle="lightbox" style="margin-bottom: 20px;" href="<?php echo site_url("uploads/{$each_pic->album_id}/{$each_pic->img_name}"); ?>">
                <?php $filename = "./uploads/{$each_pic->album_id}/{$each_pic->img_name}"; ?>
                <?php $url = site_url("uploads/{$each_pic->album_id}/{$each_pic->img_name}"); ?>
                <?php $dim = "original" ?>
                <?php if (is_file($filename)): ?>
                    <?php $info = getimagesize($filename); ?>
                    <?php if ($info[0] > $info[1]): ?>
                        <?php $dim = "landscape"; ?>
                    <?php else: ?>
                        <?php $dim = "portrait"; ?>
                    <?php endif; ?>
                <?php endif; ?>
                <div class="holder_album <?php echo $dim; ?>" <?php echo $dim != "original" ? "style='background-image:url(\"{$url}\");'" : ""; ?> ></div>
            </a>
        <?php endforeach; ?>
    </div>
</div>
<script>
    $(function() {
        $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    });
</script>