<div class="container" style="margin-top: 25px;">
    <p><?php echo image_asset("Untitled-1_05.png"); ?></p>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12 txt_right">
                    <ul class="pagination pagination-sm" style="margin: 23px 0px 12px;">
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo current_url() . "?page={$i}" ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
            <?php foreach ($guestbook as $person): ?>
                <div class="row each_comment">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="panel-title">
                                    <i class="glyphicon glyphicon-user"></i> <?php echo $person->guest_name; ?>
                                </span>
                            </div>
                            <div class="panel-body"><?php echo auto_link($this->typography->auto_typography($person->guest_detail, true)); ?></div>
                            <div class="panel-footer txt_right">
                                <b><i><?php echo date('D, j F y H:i', $person->guest_timestamp); ?></i></b>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="row">
                <div class="col-md-12 txt_right">
                    <ul class="pagination pagination-sm" style="margin: 0px;">
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo current_url() . "?page={$i}" ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h1 class="orange carbon txt_upper"><i>leave your comment</i></h1>
            <form action="<?php echo site_url('tours/comment'); ?>" method="post" id="form_comment">
                <div class="form-group">
                    <input type="text" name="guest-name" placeholder="Name" required="required" class="form-control">
                </div>
                <div class="form-group">
                    <input type="email" name="guest-email" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <textarea name="guest-comment" placeholder="Detail" required="required" rows="9" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-warning" id="submit_btn" type="submit" data-loading-text="Submiting..."><i class="glyphicon glyphicon-check"></i> Submit</button>
                    <button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-refresh"></i> Clear</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-sm" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Status report</h4>
            </div>
            <div class="modal-body">
                <p id="status_report_txt">One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function() {
        var f = false;
        $('#form_comment').attr('action', '<?php echo site_url('api/comment'); ?>');
        $('#form_comment').ajaxForm({
            beforeSend: function() {
                $('#submit_btn').button('loading');
                f = false;
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    $('#status_report_txt').text('Thank you for your comment');
                    f = true;
                } else {
                    $('#status_report_txt').text('Cannot process your request');
                }
                $('#status_report').modal();
                $('#submit_btn').button('reset');
            }
        });

        $('#status_report').on('hidden.bs.modal', function(e) {
            if(f){
                location.reload();
            }
        });
    });
</script>