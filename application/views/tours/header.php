<section class="menu">
    <div class="container">
        <div class="row">
            <a href="<?php echo site_url(); ?>" class="col-md-4 relative logo_site hidden-sm hidden-xs">
                <img src="<?php echo image_asset_url('header_logo_mouseover.png'); ?>" class="img-responsive">                
            </a>
            <div class="col-md-8 relative margin_top_menu col-sm-12 col-xs-12 ">
                <div class="row">
                    <div>
                        <div class="dropdown lang_btn pull-right">
                            <button class="btn btn-warning dropdown-toggle" type="button" id="language_holder" data-toggle="dropdown">
                                <span class=" hidden-sm hidden-xs">
                                    <i class="glyphicon glyphicon-globe"></i>&nbsp;<span id="land_placeholder">Language</span>
                                    <span class="caret"></span>
                                </span>
                                <span class="hidden-lg hidden-md">
                                    <i class="glyphicon glyphicon-globe"></i>
                                    <span class="caret"></span>
                                </span>
                            </button>
                            <ul class="dropdown-menu txt_left" role="menu" aria-labelledby="language_holder">
                                <?php foreach ($all_lang as $each_lang): ?>
                                    <li role="presentation" class="each_lang <?php echo $current_lang == "lang_" . $each_lang->lang_id ? "active" : ""; ?>"><a role="menuitem" tabindex="-1" href="#lang_<?php echo $each_lang->lang_id; ?>" data-lang="<?php echo $each_lang->lang_id; ?>"><?php echo $each_lang->lang_name; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="dropdown menu_btn hidden-lg hidden-md pull-right" style="margin: 0px 10px;">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-align-justify"></i>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu txt_upper txt_left" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation" class="<?php echo site_url() == current_url() ? "active" : ""; ?>"><a href="<?php echo site_url(); ?>">home</a></li>
                                <li role="presentation" class="<?php echo site_url('tours/aboutus') == current_url() ? "active" : ""; ?>"><a href="<?php echo site_url('tours/aboutus'); ?>">about us</a></li>
                                <li role="presentation" class="<?php echo ((site_url('tours/trip') == current_url()) || ($trip_menu) == true) ? "active" : ""; ?>"><a href="<?php echo site_url('tours/trip'); ?>">trip detail</a></li>
                                <li role="presentation" class="<?php echo ((site_url('tours/gallery') == current_url()) || ($gallery_menu) == true) ? "active" : ""; ?>"><a href="<?php echo site_url('tours/gallery'); ?>">gallery</a></li>
                                <li role="presentation" class="<?php echo site_url('tours/guestalbum') == current_url() ? "active" : ""; ?>"><a href="<?php echo site_url('tours/guestalbum'); ?>">guest album</a></li>
                                <!-- <li role="presentation" class="<?php echo site_url('tours/hostel') == current_url() ? "active" : ""; ?>"><a href="<?php echo site_url('tours/hostel'); ?>">Hostel</a></li> -->
                                <li role="presentation" class="<?php echo site_url('tours/guestbook') == current_url() ? "active" : ""; ?>"><a href="<?php echo site_url('tours/guestbook'); ?>">guestbook</a></li>
                                <li role="presentation" class="<?php echo site_url('tours/contactus') == current_url() ? "active" : ""; ?>"><a href="<?php echo site_url('tours/contactus'); ?>">contact us</a></li>
                            </ul>
                        </div>

                        <div class="hidden-lg hidden-md pull-right">
                            <h3 class="carbon orange txt_upper" style="margin-top: 5px;">Chiangmai Jungle Trekking</h3>
                        </div>
                    </div>
                </div>

                <!-- Desktop Menu-->
                <ul class=" txt_upper menumain gray hidden-sm hidden-xs">
                    <li><a href="<?php echo site_url(); ?>" class="<?php echo site_url() == current_url() ? "active" : ""; ?>">home</a></li>
                    <li><a href="<?php echo site_url('tours/trip'); ?>" class="<?php echo site_url('tours/trip') == current_url() ? "active" : ""; ?>">Trip Detail</a></li>
                    <li><a href="<?php echo site_url('tours/gallery'); ?>" class="<?php echo site_url('tours/gallery') == current_url() ? "active" : ""; ?>">Gallery</a></li>
                    <li><a href="<?php echo site_url('tours/guestalbum'); ?>" class="<?php echo site_url('tours/guestalbum') == current_url() ? "active" : ""; ?>">Guest Album</a></li>
                    <!-- <li>
                        <div class="dropdown">
                            <a data-toggle="dropdown" href="#" class="gray <?php echo ((site_url('tours/gallery') == current_url()) || ($gallery_menu) == true) ? "active" : ""; ?> dropdown-toggle">
                                Gallery
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li>
                                    <a class="default_font" href="<?php echo site_url('tours/gallery'); ?>">gallery</a>
                                </li>

                                <li>
                                    <a class="default_font" href="<?php echo site_url('tours/guestalbum'); ?>">guest album</a>
                                </li>
                            </ul>
                        </div>
                    </li> -->
                    <!-- <li><a href="<?php echo site_url('tours/hostel'); ?>" class="<?php echo site_url('tours/hostel') == current_url() ? "active" : ""; ?>">Hostel</a></li> -->
                    <li><a href="<?php echo site_url('tours/guestbook'); ?>" class="<?php echo site_url('tours/guestbook') == current_url() ? "active" : ""; ?>">guestbook</a></li>
                    <li><a href="<?php echo site_url('tours/contactus'); ?>" class="<?php echo site_url('tours/contactus') == current_url() ? "active" : ""; ?>">contact us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <script type='text/javascript'>
        $(function() {
            var lang = $('.each_lang.active a').text();
            $('#land_placeholder').text(lang);

            $('.each_lang a').click(function() {
                var lang_id = $(this).attr('data-lang');
                $.get('<?php echo site_url('api/set_lang'); ?>', {lang_id: lang_id}, function(res) {
                    if (res.status === 'success') {
                        location.reload();
                    } else {
                        alert('Request time out');
                    }
                }, 'json');
                return false;
            });

            $('.logo_site').mouseenter(function() {
                $(this).find('img').stop().animate({opacity: 1}, 400);
            }).mouseleave(function() {
                $(this).find('img').stop().animate({opacity: 0}, 400);
            });
        });
    </script>
</section>