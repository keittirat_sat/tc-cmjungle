<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p style="margin: 50px 0px;"><img src="<?php echo image_asset_url('hostel_cover.jpg'); ?>" class="img-fit-screen" style="margin: auto auto;"></p>

            <h1 class="carbon orange">Toto Hostel</h1>

            <p>Welcome to Toto Hostel Chiang Mai, the brand new boutique mini-Hostel and part of the Chiang Mai Elephant Land, Chiang Mai Jungle Trekking and Toto Elephant Sanctuary group.</p>
            <p>Overlooking the Old City moat, we are a short walk to Saturday and Sunday Walking Streets, Chiang Mai Night Bazaar and the nightlife areas. We are also close to the Airport and the best shopping. Opposite the Hostel is the local market where you can find delicious Thai food.</p>
            <p>We offer free Wifi, Continental/Thai/American breakfast and good coffee.</p>
            <p>All our rooms are air-conditioned, with shared shower and bathroom, the newest, comfiest beds and pillows, 24-Hour Reception/ Security.</p>
            <p>Our Tour Desk can help you with all you bookings and our friendly staff can give you all the best advice on where to go and what to see in Chiang Mai. We are here to help and look forward to welcoming you.</p>

            <div class="hostel_promotion">
                <h1 class="carbon orange">*** Special price please contact ***</h1>
                <p>TotoHostelChiangmai<span>@gmail.com</span></p>
            </div>

            <h1 class="carbon orange">Room Information</h1>

            <div class="jumbotron hostel-room">
                <h2 class="carbon orange">Double bed room</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <img class="img-responsive" src="<?php echo image_asset_url('hostel/double_bed/Double room_190616_0003.jpg') ?>" />
                    </div>
                    <div class="col-sm-9">
                        <h3 class="carbon orange">Information</h3>
                        <table class="carbon">
                            <tr>
                                <td><i class="fas fa-male"></i></td>
                                <td>2 Guests</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-bed"></i></td>
                                <td>1 double bed</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-wind"></i></td>
                                <td>Air Condition</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-wifi"></i></td>
                                <td>Free WiFi</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-coffee"></i></td>
                                <td>Breakfast THB 100 (Optional)</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-restroom"></i></td>
                                <td>Shared - Bathroom</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="jumbotron hostel-room">
                <h2 class="carbon orange">Tripple bed room</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <img class="img-responsive" src="<?php echo image_asset_url('hostel/tripple_bed/Tripple room_190616_0004.jpg') ?>" />
                    </div>
                    <div class="col-sm-9">
                        <h3 class="carbon orange">Information</h3>
                        <table class="carbon">
                            <tr>
                                <td><i class="fas fa-male"></i></td>
                                <td>3 Guests</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-bed"></i></td>
                                <td>1 bunk bed + 1 single bed</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-wind"></i></td>
                                <td>Air Condition</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-wifi"></i></td>
                                <td>Free WiFi</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-coffee"></i></td>
                                <td>Breakfast THB 100 (Optional)</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-restroom"></i></td>
                                <td>Shared - Bathroom</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="jumbotron hostel-room">
                <h2 class="carbon orange">Mixed dorm</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <img class="img-responsive" src="<?php echo image_asset_url('hostel/mixed_dorm/Mixed Dorm_190616_0004.jpg') ?>" />
                    </div>
                    <div class="col-sm-9">
                        <h3 class="carbon orange">Information</h3>
                        <table class="carbon">
                            <tr>
                                <td><i class="fas fa-male"></i></td>
                                <td>4 Guests</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-bed"></i></td>
                                <td>2 Bunk bed</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-wind"></i></td>
                                <td>Air Condition</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-wifi"></i></td>
                                <td>Free WiFi</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-coffee"></i></td>
                                <td>Breakfast THB 100 (Optional)</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-restroom"></i></td>
                                <td>Shared - Bathroom</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-city"></i></td>
                                <td>City view</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="jumbotron hostel-room">
                <h2 class="carbon orange">Facilities</h2>
                <div class="row">
                    <?php for($i = 1; $i <= 10; $i++): ?>
                    <div class="col-sm-4">
                        <div class="gallery-item" style="background-image: url(<?php echo image_asset_url("hostel/others/{$i}.jpg")?>)">
                            <img src="<?php echo image_asset_url("hostel-gallery.png")?>" alt="" class="img-responsive">
                        </div>
                    </div>
                    <?php endfor ?>
                </div>
            </div>

        </div>
    </div>
</div>
<style>
    h1.page_title {
        font-size: 60px;
        text-shadow: 1px 1px 3px #000;
    }

    p.txt{
        text-indent: 30px;
    }

    img.img-fit-screen {
        width: 100%;
    }

    .jumbotron.hostel-room {
        padding: 25px 25px 25px 25px;
    }

    .jumbotron.hostel-room h2 {
        margin-top: 0px;
    }

    .jumbotron.hostel-room h3 {
        margin-top: 0px;
        margin-bottom: 5px;
    }

    .jumbotron.hostel-room i {
        margin-right: 15px;
    }

    .gallery-item {
        margin-bottom: 30px;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
    }

    .hostel_promotion {
        background-color: rgba(255,255,255,0.5);
        border: 1px solid #666;
        margin: 35px 0px;
        padding: 25px 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }

    .hostel_promotion p {
        font-weight: bold;
        font-size: 16px;
    }

    .hostel_promotion p span {
        color: #666;
        font-weight: normal;
    }
</style>