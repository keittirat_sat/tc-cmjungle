<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p style="margin-top: 25px;"><?php echo image_asset('About_01.png'); ?></p>
            <p><img src="<?php echo image_asset_url('About_banner.png'); ?>" class="img-responsive" style="margin: auto auto;"></p>
            <?php $l1 = "Welcome to Chiang Mai Jungle Trekking, My name is Toto, although some people call me “Pumpkin Head”. I have 15 years of trekking experience in and around the Hill Tribes of Northern Thailand - from Pai to Mae Hong Son, the Golden Triangle and anywhere else you want to go."; ?>
            <?php $l2 = "I was born in a small Lisu village in the beautiful Chiang Dao Forest, and regularly take small groups back to my Hill Tribe home to experience real jungle life – staying in real tribal huts, with real tribal people, and real tribal food; cooked using authentic methods."; ?>
            <?php $l3 = "At Chiang Mai Jungle Trekking, we offer many styles of treks, depending on how much time you have and what you really want to do. Whether it’s riding Elephants, Bamboo Rafting, exploring caves or the amazing jungle and rainforest, we personally tailor treks to suit you. No matter where we go, you will always meet authentic Hill Tribe people and learn about their culture - and most of all, have fun!"; ?>
            <?php $find = "Chiang Mai Jungle Trekking"; ?>
            <p class="txt"><?php echo highlight_phrase($l1, $find, "<a href='" . site_url() . "' class='orange'>", "</a>") ?></p>
            <p class="txt"><?php echo highlight_phrase($l2, $find, "<a href='" . site_url() . "' class='orange'>", "</a>") ?></p>
            <p class="txt"><?php echo highlight_phrase($l3, $find, "<a href='" . site_url() . "' class='orange'>", "</a>") ?></p>
            <p class="txt_right"><?php echo image_asset("Untitled-1_07.png"); ?></p>
        </div>
    </div>
</div>
<style>
    p.txt{
        text-indent: 30px;
    }
</style>