<section id="footer">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-1 hidden-sm hidden-xs">
                    <a href="<?php echo site_url(); ?>">
                        <img src="<?php echo image_asset_url('footer_logo.png') ?>" class="img-responsive">
                    </a>
                </div>
                <div class="col-md-8">
                    <p style="font-style: italic;" class="white">
                        <strong class="orange">Address:</strong><br />
                        <!-- <strong><?php echo address_name_1 ?></strong><br /> -->
                        <!-- <?php echo address_1 ?><br /> -->
                        <strong><?php echo address_name_2 ?></strong><br />
                        <?php echo address_2 ?>
                    </p>
                    <p style="font-style: italic;" class="white">
                        <strong class="orange">Phone:</strong><br />
                        +66 (0)81 035 4836
                    </p>
                    <p style="font-style: italic;" class="white">
                        <strong class="orange">WhatsApp:</strong><br />
                        +66 (0)84 790 4675
                    </p>
                    <p style="font-style: italic;" class="white">
                        <strong class="orange">Email:</strong><br />
                        chiangmaijungletrekking@gmail.com
                    </p>
                    <p style="font-style: italic;" class="white">
                        <strong class="orange">License:</strong><br />
                        Guide Licensed #23-0389 and Company license 24/00780
                    </p>
                    <p class="hidden-sm hidden-xs">
                        <a href="<?php echo FB_FANPAGE; ?>"><img src="<?php echo image_asset_url('footer_fb.png'); ?>"></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=194531797284411";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-42013443-8', 'chiangmaijungletrekking.com');
    ga('send', 'pageview');

</script>