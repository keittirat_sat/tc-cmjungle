<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="col-md-12">
            <p><?php echo image_asset("Untitled-1_06.png"); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12 txt_right">
                    <ul class="pagination pagination-sm" style="margin: 23px 0px 12px;">
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo current_url() . "?page={$i}" ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
            <?php foreach ($review as $each_review): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span class="panel-title"><i class="glyphicon glyphicon-flag"></i> <?php echo $each_review->link_title; ?></span>
                            </div>
                            <div class="panel-body">
                                <i><?php echo $each_review->link_detail; ?>&hellip;</i>
                            </div>
                            <div class="panel-footer txt_right">
                                <?php $slug = url_title($each_review->link_title); ?>
                                <?php $slug = (trim($slug) == "") ? urlencode($each_review->link_title) : $slug; ?>
                                <?php echo trim($each_review->link_author) != "" ? $each_review->link_author : "Anonymous"; ?>&nbsp;|&nbsp;<a target="_blank" class="btn btn-warning btn-xs" href="<?php echo $each_review->folder == 0 ? $each_review->link : site_url("tours/pin_review/{$each_review->link_id}/{$slug}"); ?>"><i class="glyphicon glyphicon-link"></i> VIEW</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="row">
                <div class="col-md-12 txt_right">
                    <ul class="pagination pagination-sm" style="margin: 0px;">
                        <?php for ($i = 0; $i < $total_page; $i++): ?>
                            <li <?php echo $i == $page ? "class='active'" : ""; ?>><a href="<?php echo current_url() . "?page={$i}" ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <?php echo!empty($sidebar) ? $sidebar : ""; ?>
        </div>
    </div>
</div>