<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="col-md-12">
            <p><?php echo image_asset("Untitled-1_06.png"); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h1 class="orange carbon"><i><?php echo $post->link_title; ?></i></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <li><a href="<?php echo site_url('tours/review'); ?>">Referer Review</a></li>
                <li class="active"><?php echo $post->link_title; ?></li>
            </ol>
            <p class="txt_center"><img src="<?php echo site_url("uploads/{$post->folder}/{$post->filename}"); ?>"></p>
            <?php echo $this->typography->auto_typography($post->link_detail); ?>
            <p class="txt_right"><a href="<?php echo $post->link ?>" class="btn btn-warning"><i class="glyphicon glyphicon-link"></i> Go to site</a></p>
        </div>
        <div class="col-md-4">
            <?php echo!empty($sidebar) ? $sidebar : ""; ?>
        </div>
    </div>
</div>