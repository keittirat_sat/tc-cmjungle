<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="col-md-12">
            <p><?php echo image_asset("Untitled-1_02.png"); ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-5">
            <p class="txt_center" style="margin-bottom: -37px;"><?php echo image_asset("about_trip.png"); ?></p>
            <div class="trip_cover">
                <?php foreach ($post as $each_trip): ?>
                    <?php $slug = url_title($each_trip->post_title); ?>
                    <?php $slug = (trim($slug) == "") ? urlencode($each_trip->post_title) : $slug; ?>
                    <?php $post_content = (array) json_decode($each_trip->post_detail); ?>
                    <div class="hidden-sm hidden-xs">
                        <h3 class="carbon" style="margin-bottom: 0px;">
                            <a class="orange" href="<?php echo site_url("tours/trip/{$each_trip->post_id}/{$slug}"); ?>">
                                <i><?php echo $each_trip->post_title; ?></i>
                            </a>
                        </h3>
                        <span class="white"><?php echo mb_substr(strip_tags($post_content[$current_lang]), 0, 30, 'utf-8'); ?>&hellip;</span>
                    </div>
                <?php endforeach; ?>
                
                <div class="dropdown">
                    <button class="btn dropdown-toggle hidden-lg hidden-md" type="button" id="dropdownMenu1" data-toggle="dropdown" style="width: 100%; margin-top: 15px;">
                        All Program
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <?php foreach ($post as $each_trip): ?>
                            <?php $slug = url_title($each_trip->post_title); ?>
                            <?php $slug = (trim($slug) == "") ? urlencode($each_trip->post_title) : $slug; ?>
                            <?php $post_content = (array) json_decode($each_trip->post_detail); ?>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo site_url("tours/trip/{$each_trip->post_id}/{$slug}"); ?>"><?php echo $each_trip->post_title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-7 web_format">
            <?php if (count($post)): ?>
                <?php $current_post = false; ?>
                <?php if ($post_id != null): ?>
                    <?php foreach ($post as $each_trip): ?>
                        <?php if ($each_trip->post_id == $post_id): ?>
                            <?php $current_post = $each_trip; ?>
                            <?php break; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php $current_post = $post[0]; ?>
                <?php endif; ?>

                <?php if ($current_post): ?>
                    <h1 class="orange carbon" style="margin-top: 10px;"><i><?php echo $current_post->post_title; ?></i></h1>
                    <?php $post_content = (array) json_decode($current_post->post_detail); ?>
                    <?php echo $this->typography->auto_typography($post_content[$current_lang]); ?>
                <?php else: ?>
                    <h1 class="orange carbon txt_center txt_shadow"><?php echo $this->typography->auto_typography("--" . $lang_info->lang_info . "--"); ?></h1>
                <?php endif; ?>
            <?php else: ?>
                <h1 class="orange carbon txt_center txt_shadow"><?php echo $this->typography->auto_typography("--" . $lang_info->lang_info . "--"); ?></h1>
            <?php endif; ?>
        </div>
    </div>
</div>