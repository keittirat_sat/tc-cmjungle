<div class="container" style="margin-top: 25px;">
    <div class="row">
        <div class="col-md-12">
            <p><?php echo image_asset("Untitled-1_03.png"); ?></p>
        </div>
    </div>
    <div class="row">
        <?php foreach ($album as $each_album): ?>
            <?php $slug = url_title($each_album['album_name']); ?>
            <?php $slug = (trim($slug)=="") ? urlencode($each_album['album_name']) : $slug; ?>
            <a class="col-lg-3 col-md-4 col-sm-6" data-slug="<?php echo $slug; ?>" style="margin-bottom: 20px;" href="<?php echo site_url("tours/album/{$each_album['album_id']}/{$slug}"); ?>">
                <?php $thumb = $each_album['image'][0]; ?>
                <?php $filename = "./uploads/{$each_album['album_id']}/{$thumb->img_name}"; ?>
                <?php $url = site_url("uploads/{$each_album['album_id']}/{$thumb->img_name}"); ?>
                <?php $dim = "original" ?>
                <?php if (is_file($filename)): ?>
                    <?php $info = getimagesize($filename); ?>
                    <?php if ($info[0] > $info[1]): ?>
                        <?php $dim = "landscape"; ?>
                    <?php else: ?>
                        <?php $dim = "portrait"; ?>
                    <?php endif; ?>
                <?php endif; ?>
                <div class="holder_album <?php echo $dim; ?>" <?php echo $dim != "original" ? "style='background-image:url(\"{$url}\");'" : ""; ?> >
                    <p class="holder_title orange"><?php echo $each_album['album_name']; ?></p>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</div>