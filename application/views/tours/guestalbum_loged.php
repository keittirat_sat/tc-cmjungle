<div class="container">

    <p style="margin-top: 25px;"><?php echo image_asset('Untitled-1_03.png'); ?></p>
    <div class="row">
        <?php if (count($all_album) > 0): ?>

            <?php foreach ($all_album as $each_album): ?>
                <div class="col-md-3 col-sm-4" style="margin-bottom: 20px;">
                    <a href="<?php echo site_url("tours/fullsize?album_id={$each_album->id}"); ?>" target="_blank" class="thumbnail">
                        <img src="<?php echo $each_album->gallery_picture->thumb; ?>">
                        <div class="caption">
                            <h4><?php echo $each_album->name; ?></h4>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>

        <?php else: ?>
            <h3 class="text-center">Not found any album</h3>
        <?php endif; ?>
    </div>
    
</div>