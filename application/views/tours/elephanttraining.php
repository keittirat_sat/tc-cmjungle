<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p style="margin-top: 25px;"><?php echo image_asset('Untitled-1_08.png'); ?></p>
            <p><img src="<?php echo image_asset_url('elephanttraining_banner.png'); ?>" class="img-responsive" style="margin: auto auto;"></p>
            <h2 class="carbon orange">Elephant Training course</h2>
            <p>Trip include  :  FREE download photos, transportation, uniform, lunch, tourist guide, accident insurance, national park ‘s entrance fee</p>

            <h2 class="carbon orange">Pricing (1 Elephant / 2 Person)</h2>
            <ul>
                <li>2,500 THB/Person</li>
                <li>Children under 10 years 1,800 THB</li>
            </ul>

            <h2 class="carbon orange">Itinerary</h2>
            <p>08.00-08.30 am pick up from your hotel</p>
            <ul>
                <li>Change clothes and start to learn about elephant habits and lifestyles</li>
                <li>Learn natural herbal nutrition for elephant, 1 plant from you for sustainable elephant ‘s food. </li>
                <li>Learn and practise elephant ‘s command & how to     ride the elephant</li>
                <li>Time for a great lunch</li>
                <li>Start to control and ride your elephant (bare back)     through the valley and jungle. </li>
                <li>Bath & brush your elephant</li>
                <li>Then Toto will take you to Mok Fah Waterfall at     National Park to enjoy and relax your Good Day    in a quite, beautiful place.</li>
                <li>18.00-18.30 Arrive Chiang Mai</li>
            </ul>

            <h2 class="carbon orange">Things to bring</h2>
            <ol>
                <li>Hat</li>
                <li>Extra clothes </li>
                <li>Bikini</li>
                <li>Towel</li>
                <li>Sandals</li>
                <li>Sun block lotion</li>
                <li>Insect spray</li>
            </ol>

            <p class="txt_right"><?php echo image_asset("Untitled-1_07.png"); ?></p>
        </div>
    </div>
</div>
<style>
    p.txt{
        text-indent: 30px;
    }
</style>