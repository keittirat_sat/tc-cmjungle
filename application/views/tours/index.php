<section id="display">
    <!--.orange_ribbon-->
    <div class="orange_ribbon">
        <div class=container>
            <div class="row">
                <div class="col-md-12"><div class="slider-wrapper theme-default">
                        <div class="ribbon"></div>
                        <div class="nivoSlider" id="slider">
                            <?php foreach ($slide as $each_slide): ?>
                                <img src="<?php echo $each_slide['url']; ?>">
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.orange_ribbon-->

    <div class="container">

        <div class='row'>
            <div class='col-md-4 col-md-offset-1 txt_right'>
                <img src="<?php echo image_asset_url("greeting.png") ?>" class="img-responsive" style="margin: 0px auto;">
            </div>
            <div class="col-md-6 welcome_text gray">
                <p class="title_welcome">
                    <img src="<?php echo image_asset_url("Untitled-1_01.png"); ?>" class="img-responsive">
                </p>


                <?php $index_welcome_txt = [
                    "Welcome to Chiang Mai Jungle Trekking. My name is Toto",
                    "I was born in a small Lisu village in the beautiful Chiang Dao Forest and, for 25 years, I regularly take small groups back to my traditional Hill Tribe home to experience the true jungle life _ staying in real tribal huts, with real tribal people, eating real tribal food cooking using the traditional methods I fondly remember as a village child.",
                    "As well as our jungle trekking adventures, we operate a wonderful elephant sanctuary, dedicated to the care and preservation of these amazing, intelligent and loveable giants of the jungle.",
                    "Based on our 25 years of experience and our unique hill tribe relationships, we have carefully created a variety of choices that we know will more than fulfill your expectations.",
                    "We invite you to click……",
                    "Toto’s Elephant Sanctuary. We offer you a range of wonderful and unique experiences that you will never forget and will want to share with your friends. Observe the elephants in their natural environment, feeding and caring for their young and if you wish, go trekking through the rainforest. Get close to these amazing creatures but in an ethical and eco-friendly manner.",
                    // "Toto Hostel, bed and breakfast, clean, good price, located in city around the moat, closed with morning market, street food at night, weekend market, popular temples Etc.",
                    "Chiangmai Elephant Land, good location for elephants on which to roam, rich forests to enjoy, beautiful waterfalls and rivers in which to bathe and relax, and provision of organic, natural foods.",
                ]; ?>

                <?php foreach($index_welcome_txt as $line_text): ?>
                    <p>
                        <?php echo highlight_multi_phrase($line_text, array(
                            array('phrase' => 'Chiang Mai Jungle Trekking', 'tag_open' => "<a href='" . site_url() . "' class='orange'>", 'tag_close' => "</a>"),
                            array('phrase' => "Toto’s Elephant Sanctuary", 'tag_open' => "<a href='//www.totoelephantsanctuary.com' target='_blank' class='orange'>", 'tag_close' => "</a>"),
                            array('phrase' => "Chiangmai Elephant Land", 'tag_open' => "<a href='//chiangmaielephantland.com' target='_blank' class='orange'>", 'tag_close' => "</a>"),
                        )) ?>
                    </p>
                <?php endforeach; ?>

                <p class="txt_right"><?php echo image_asset("Untitled-1_07.png"); ?></p>
            </div>
        </div>

        <div class='row' style="margin-top: 35px;">
            <div class='col-md-8'>
                <p class='txt_center' style='margin-bottom: -37px; z-index: 9; position: relative;'><img src="<?php echo image_asset_url("about_trip.png") ?>"></p>
                <div class='row'>
                    <div class="col-md-12" style="background-color: #999; padding: 50px 15px 15px;">
                        <?php if (count($post)): ?>
                            <?php foreach ($post as $each_post): ?>
                                <div class="row" style="margin-bottom: 20px;">
                                    <div class="col-md-3">
                                        <?php $thumb = $this->post->get_image_by_id($each_post->post_thumbnail); ?>
                                        <?php if ($thumb): ?>
                                            <img src="<?php echo $thumb['mini_url']; ?>" class="img-responsive">
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-md-9">
                                        <h2 style="margin: 0px;" class="carbon orange"><i><?php echo $each_post->post_title; ?></i></h2>
                                        <p class="white">
                                            <?php $post_content = (array) json_decode($each_post->post_detail); ?>
                                            <?php echo mb_substr(strip_tags($post_content[$current_lang]), 0, 240, 'utf-8'); ?>&hellip;
                                        </p>
                                        <p class="txt_right">
                                            <a href="<?php echo site_url('tours/trip/' . $each_post->post_id . '/' . url_title($each_post->post_title)); ?>">
                                                <?php echo image_asset("btu_readmore.png") ?>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <h4 class="txt_center"><?php echo $lang_info->lang_info; ?></h4>
                        <?php endif; ?>
                    </div>
                </div>
                
<!--                <div class='row' style='margin-top: 50px;'>-->
<!--                    <div class='col-md-12'>-->
<!--                        <a href='--><?php //echo site_url('tours/elephanttraining'); ?><!--'>-->
<!--                            <img src='--><?php //echo image_asset_url("elephant_banner.png") ?><!--' class='img-responsive' style='width: 100%;'>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                -->
<!--                <div class="row" style="margin-top: 10px;">-->
<!--                    <div class="col-xs-12">-->
<!--                        <iframe width="640" height="360" src="https://www.youtube.com/embed/ID-jAuGHwHE" frameborder="0" allowfullscreen style="display: block; margin: 0 auto;"></iframe>-->
<!--                    </div>-->
<!--                </div>-->
                
                <div class='row' style='margin-top: 50px;'>
                    <div class='col-md-12'>
                        <a href='<?php echo site_url('tours/contactus'); ?>'>
                            <img src='<?php echo image_asset_url("contactus_banner.png") ?>' class='img-responsive' style='width: 100%;'>
                        </a>
                    </div>
                </div>   
                
                <div class='row' style='margin-top: 50px;'>
                    <div class='col-md-12'>
                        <a href='//chiangmaielephantland.com/'>
                            <img src='<?php echo image_asset_url("cmjungle_banner_01_001.png") ?>' class='img-responsive' style='width: 100%;'>
                        </a>
                    </div>
                </div>      
                
                <div class='row' style='margin-top: 50px;'>
                    <div class='col-md-12'>
                        <a href='//www.tripadvisor.com/Attraction_Review-g293917-d6420074-Reviews-Chiangmai_Jungle_Trekking_Private_Day_Tours-Chiang_Mai.html'>
                            <img src='<?php echo image_asset_url("Tripadvisor_tag.png") ?>' class='img-responsive' style='width: 100%;'>
                        </a>
                    </div>
                </div>
            </div>
            <div class='col-md-4'>
                <?php echo!empty($sidebar) ? $sidebar : ""; ?>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function() {
        $('#slider').nivoSlider();
    });
</script>