<section id="display">
    <!--.orange_ribbon-->
    <div class="orange_ribbon">
        <div class=container>
            <div class="row">
                <p class='txt_center' style='margin: 25px 0px -38px; position: relative; z-index: 9;'>
                    <img src="<?php echo image_asset_url('contactus_tag.png'); ?>">
                </p>
                <div class="col-md-12" id='map_canvas' style='height: 360px;'></div>
            </div>
        </div>
    </div><!--/.orange_ribbon-->

    <div class='container' style='margin-top: 20px;'>
        <div class='row'>
            <div class='col-md-5 col-md-offset-1'>
                <p style="font-style: italic;">
                    <strong class="orange">Address:</strong><br />
                    <!-- <strong><?php echo address_name_1 ?></strong><br /> -->
                    <!-- <?php echo address_1 ?><br /> -->
                    <strong><?php echo address_name_2 ?></strong><br />
                    <?php echo address_2 ?>
                </p>
                <p style="font-style: italic;">
                    <strong class="orange">Phone:</strong><br />
                    +66 (0)81 035 4836
                </p>
                <p style="font-style: italic;">
                    <strong class="orange">WhatsApp:</strong><br />
                    +66 (0)84 790 4675
                </p>
                <p style="font-style: italic;">
                    <strong class="orange">Email:</strong><br />
                    chiangmaijungletrekking@gmail.com
                </p>
                <p style="font-style: italic;">
                    <strong class="orange">License:</strong><br />
                    Guide Licensed #23-0389 and Company license 24/00780
                </p>
            </div>
            <div class='col-md-5'>
                <form role='form' method='post' class="form-contact" action="<?php echo site_url('tours/mail'); ?>">
                    <div class='form-group'>
                        <input type='text' class='form-control' name='contact-name' placeholder="Name" required="required">
                    </div>
                    <div class='form-group'>
                        <input type='email' class='form-control' name='contact-email' placeholder="Email" required="required">
                    </div>
                    <div class='form-group'>
                        <input type='text' class='form-control' name='contact-whatsapp' placeholder="Whatsapp">
                    </div>
                    <div class='form-group'>
                        <input type='text' class='form-control' name='contact-subject' placeholder="Subject" required="required">
                    </div>
                    <div class='form-group'>
                        <textarea class='form-control' name='contact-detail' placeholder="Message" rows="12" required="required"></textarea>
                    </div>
                    <div class='form-group txt_right'>
                        <button class="btn btn-warning" type="submit" data-loading-text="Sending..." id="submit_btn"><i class="glyphicon glyphicon-envelope"></i> Submit</button>
                        <button class="btn" type="reset"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="status_report">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Contact form</h4>
            </div>
            <div class="modal-body">
                <p id="status_report_txt">&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="glyphicon glyphicon-ok"></i> Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  $(function () {
      $('#map_canvas').gmap({
          'mapTypeId': google.maps.MapTypeId.HYBRID,
          'zoom': 14,
          'center': '18.789686,98.993285'
      }).bind('init', function (ev, map) {
        //   $('#map_canvas').gmap('addMarker', {'position': '18.7808826,98.9911467', 'icon': '<?php echo image_asset_url("contactus_mappin.png") ?>'}).click(function () {
        //       var marker = "<h3 class='orange txt_shadow'><?php echo address_name_1 ?></h3>";

        //       marker += "<p><?php echo address_1 ?></p>";

        //       $('#map_canvas').gmap('openInfoWindow', {'content': marker}, this);
        //   });

          $('#map_canvas').gmap('addMarker', {'position': '18.7855392274809, 98.99345481043343', 'icon': '<?php echo image_asset_url("contactus_mappin.png") ?>'}).click(function () {
              var marker = "<h3 class='orange txt_shadow'><?php echo address_name_2 ?></h3>";

              marker += "<p><?php echo address_2?></p>";

              $('#map_canvas').gmap('openInfoWindow', {'content': marker}, this);
          });
      });

      $('.form-contact').attr('action', '<?php echo site_url('api/sendmail'); ?>');

      var submit_btn;
      var f;
      $('.form-contact').ajaxForm({
          beforeSend: function () {
              submit_btn = $('#submit_btn').html();
              $('#submit_btn').attr('disabled', 'disabled').text('Sending...');
          },
          complete: function (xhr) {
              f = false;
              console.log(xhr.responseText);
              var json = $.parseJSON(xhr.responseText);
              if (json.status === "success") {
                  f = true;
                  $('#status_report_txt').text('Your request aleady sent successful');
              } else {
                  $('#status_report_txt').text('Cannot prosess your request');
              }
              $('#status_report').modal();
          }
      });

      $('#status_report').on('hidden.bs.modal', function (e) {
          if (f) {
              location.reload();
          } else {
              $('#submit_btn').removeAttr('disabled').html(submit_btn);
          }
      })
  });
</script>