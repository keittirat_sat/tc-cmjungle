<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="login_block">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title">Please Login</span>
                    </div>
                    <div class="panel-body">
                        <form method="post" class="form-horizontal" id="loginform">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Username</label>
                                <div class="col-xs-8"><input type="text" name="username" required="" class="form-control"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Password</label>
                                <div class="col-xs-8"><input type="password" name="password" required="" class="form-control"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-push-4">
                                    <button class="btn btn-primary" type="submit" data-loading-text="Loging in..." id="login_btn"><i class="glyphicon glyphicon-log-in"></i> Login</button>
                                    <button class="btn btn-default" type="reset"><i class="glyphicon glyphicon-refresh"></i> Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .login_block{
        width: 350px;
        padding: 55px 0px 15px;
        margin: 0px auto;
    }
</style>

<script>
    $(function() {
        $('#loginform').attr({'action': '/api/login'});
        $('#loginform').ajaxForm({
            beforeSend: function() {
                $('#login_btn').button("loading");
            },
            complete: function(xhr) {
                var res = xhr.responseText;
                res = $.parseJSON(res);
                if (res.status === "success") {
                    location.reload();
                } else {
                    $('#login_btn').button("reset");
                    alert("Username or Password mismatch");
                }
            }
        });
    });
</script>