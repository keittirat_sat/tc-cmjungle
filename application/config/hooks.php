<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | Hooks
  | -------------------------------------------------------------------------
  | This file lets you define "hooks" to extend CI without hacking the core
  | files.  Please see the user guide for info:
  |
  |	http://codeigniter.com/user_guide/general/hooks.html
  |
 */

//Set Timezone to Thailand
date_default_timezone_set('Asia/Bangkok');

//Defined GLOBAL var
define('GUESTBOOK_NOT_APPROVE', 0);
define('GUESTBOOK_APPROVED', 1);

define('PUBLISH_ALBUM', 1);
define('PUBLISH_GUEST', 2);
define('PRIVATE_ALBUM', 0);

define('PUBLISH_POST', 1);
define('PRIVATE_POST', 0);

define('SLIDE_IMAGE', -1);
define('POST_IMAGE', -2);

//Image type
define('IMG_FOR_ALBUM', 0);
define('IMG_FOR_POST', 1);

//Image size
define('slide_width', 1170);
define('slide_height', 460);

define('img_l_width', 1024);
define('img_l_height', 1024);

define('img_g_width', 2048);
define('img_g_height', 2048);

define('img_s_width', 250);
define('img_s_height', 250);

define('FB_FANPAGE', 'http://www.facebook.com/chiangmaijungle');

define('address_name_1', 'ToTo Hostel Chiangmai');
define('address_1', '146/7 Rat Chiang Saen Rd., T.Phra Sing, A.Mueang, Chiang Mai 50100');
define('gmap_link1', 'https://www.google.com/maps/place/Toto+Hostel+Chiangmai/@18.7808538,98.991029,15z/data=!4m2!3m1!1s0x0:0x77eba64bbb06cc50?sa=X&hl=th-TH&ved=2ahUKEwim6f6T7_blAhXOwjgGHZirAWkQ_BIwC3oECA8QCA');

define('address_name_2', 'Chiangmai Jungle Trekking');
define('address_2', '57 Kotchasarn Road, Changklan, Chiang Mai 50100');
define('gmap_link2', 'https://www.google.com/maps/place/Chiangmai+Elephant+Land/@18.7855318,98.9934646,15z/data=!4m2!3m1!1s0x0:0xcfe7eae5f18d9dcd?sa=X&hl=th-TH&ved=2ahUKEwiRl4qx7vblAhWcxzgGHSuCBqIQ_BIwGHoECA4QCA');


/* End of file hooks.php */
/* Location: ./application/config/hooks.php */