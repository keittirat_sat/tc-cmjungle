<?php

$url = "https://chiangmaijungletrekking.com/api/zip_watchdog";
$resp = file_get_contents($url);
$json = json_decode($resp);
foreach ($json as $each_album) {
    $bash = "cd /var/www/trycatch/tc-cmjungle/uploads/{$each_album->album_id}; zip -r {$each_album->album_id}.zip * -x mini_*; chmod 0777 {$each_album->album_id}.zip;";
    $run_cmd = shell_exec($bash);
    
    echo "\n---- {$each_album->album_id} ----\n{$run_cmd}\n--------\n";
}