function size_control() {
    var w = $('.box-gallery').first().width();
    $('.box-gallery').height(w);

    var main_display = (w + 8) * 6;
//    $('.bg_show_gallery').height(main_display - 8);
}

function size_control_gallery_page() {
    var w = $('.box-gallery-gen2').first().width();
    $('.box-gallery-gen2').height(w);
}

$(function() {
    size_control();
    size_control_gallery_page();

    $(window).on('resize', function() {
        if ($('.box-gallery').length > 0) {
            size_control();
            console.log('Detect 1');
        }

        if ($('.box-gallery-gen2').length > 0) {
            size_control_gallery_page();
            console.log('Detect 2');
        }
    });

    $('.medium_image_gallery').css({opacity: 0});
    $('.medium_image_gallery').first().css({opacity: 1, 'z-index': 2}).addClass('active');

    $('a.box-gallery').click(function() {
        var idx = $(this).attr('for');
//        console.log(idx);
        var active = $('.medium_image_gallery.active').attr('id');

        if (active !== idx) {
            $('#' + idx).css({'z-index': 1, opacity: 1});
            $('.medium_image_gallery.active').animate({opacity: 0}, 400, function() {
                $('.medium_image_gallery.active').css({'z-index': 0}).removeClass('active');
                $('#' + idx).addClass('active').css({'z-index': 2});
            });
        }
        return false;
    });
    ;
});